<?php
// განვსაზღვრე ცვლადი და გავუტოლე სატესტო სტრიქონს.
$test_string = 'hello world';

$replace = '/abcd/';

// MODIFIERS;
// 'i';
$exp = '/ABCD/i'; // 'i' არის მოდიფიკატორი, რომელიც აიგნორებს სიმბოლოების სიდიდეს.
echo preg_match($exp, $replace); // დააბრუნებს 1-ს.
echo '<br><br>';

// 'm';
$exp = '/ABCD/m'; // 'm' მოდიფიკატორი ასრულებს მრავალხაზოვან ძიებას.
echo preg_replace("/^[\s]*$/m","<p>","with.\n\n\n\t \nTherefor"); // დააბრუნებს სტრიქონებს.
echo '<br><br>';

// PATTERNS;
// [abc];
$string = '/GAU2022/';
echo preg_match('/[abc]/', $string); // ვპოულობთ ფრჩხილებს შორის არსებული ვარიანტიდან ერთ-ერთ სიმბოლოს სტრიქონში.
echo '<br><br>';

// [^abc];
$string = '/GAU2022/';
echo preg_match('/[^abc]/', $string); // ვპოულობთ ნებისმიერი სიმბოლოს, რომელიც არ არის ფრჩხილებს შორის მოქცეული.
echo '<br><br>';

// [0-9];
$string = '/GAU2022/';
echo preg_match('/[0-9]/', $string); // ვპოულობთ ერთ-ერთ სიმბოლოს 0-დან 9-მდე დიაპაზონიდან.
echo '<br><br>';

// 'preg_filter()';
// მოცემული სტრიქონი გამოაქვს მასივში სადაც მასში შემავალ ყოველივე ციფრს სვავს ბრეკეტებში.
$input = [
  "It is 5 o'clock",
  "40 days",
  "No numbers here",
  "In the year 2000"
];

$result = preg_filter('/[0-9]+/', '($0)', $input);
print_r($result);
echo '<br><br>';

// 'preg_grep()';
$input = [
  "Red",
  "Pink",
  "Green",
  "Blue",
  "Purple"
];

$result = preg_grep("/^p/i", $input); // მასივიდან ამოიღებს იმ ელემენტებს რომელიც იწყება 'P'-თი.
print_r($result);
echo '<br><br>';

// 'preg_last_error()';
$str = 'The regular expression is invalid.';
$pattern = '/invalid//';
$match = @preg_match($pattern, $str, $matches);

if($match === false) {
  // An error occurred
  $err = preg_last_error();
  if($err == PREG_INTERNAL_ERROR) {
    echo 'Invalid regular expression.';
  }
} else if($match) {
  // A match was found
  echo $matches[0];
} else {
  // No matches were found
  echo 'No matches found';
}
echo '<br><br>';

// 'preg_match()' search = true;
//1. იპოვის სიტყვა 'hello'-ს '$test_string' ცვლადის სტრიქონში.
echo preg_match('/hello/', $test_string); // დააბრუნებს 1-ს.
echo '<br><br>';

// 'preg_match()' search = false;
//2. იპოვის სიტყვა 'world'-ს '$test_string' ცვლადის სტრიქონში.
echo preg_match('/worldd/', $test_string); // დააბრუნებს 0-ს.
echo '<br><br>';

// '^'
// ^-ის მეშვეობით ვადგენთ იწყება თუ არა სტრიქონი სიტყვით 'hello'.
echo preg_match('/^hello/', $test_string); // დააბრუნებს 1-ს.
echo '<br><br>';

// სტრიქონის პირველი ხუთი სიმბოლო არის ალფა რიცხვითი სიმბოლო.
echo preg_match('/^[A-Za-z0-9]{5}/', $test_string); // დააბრუნებს 1-ს.
echo '<br><br>';

// 'preg_match_all()';
$pattern = '/\d+/';
$str = 'PHP 1.0 released in 1995';
if (preg_match_all($pattern, $str, $matches)) {
    print_r($matches);
}
echo '<br><br>';

// 'preg_quote()';
// ვიგებთ არის თუ არა '$input' თეგის მნიშვნელობა 'URL'.
$search = preg_quote("://", "/");
$input = 'https://www.w3schools.com/';
$pattern = "/$search/";
if(preg_match($pattern, $input)) {
  echo "The input is a URL.";
} else {
  echo "The input is not a URL.";
}
echo '<br><br>';

// 'preg_replace()';
echo preg_replace($replace, 'w', $test_string); // რომელ სტრიქონსაც ვანაცვლებთ გვერდებზე ვუწერთ სლეშს.
echo '<br><br>';

// 'preg_replace_callback()';
// ითვლის სტრიქონში შემავალი თითოეული სიტყვის სიმბოლოებს.
function countLetters($matches) {
  return $matches[0] . '(' . strlen($matches[0]) . ')';
}

$input = "Welcome to W3Schools.com!";
$pattern = '/[a-z0-9\.]+/i';
$result = preg_replace_callback($pattern, 'countLetters', $input);
echo $result;
echo '<br><br>';

// 'preg_split()';
$date = "1970-01-01 00:00:00";
$pattern = "/[-\s:]/";
$components = preg_split($pattern, $date);
print_r($components);
?>