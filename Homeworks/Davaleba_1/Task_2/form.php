<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Davaleba_1-2</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>ნიშნების უწყისი</h1>
    <p class="p1"><i>Task_2</i></p>
    <form method="post">
        <table class="table1">
            <tr>
                <td>სტუდენტის სახელი</td>
                <td>
                    <input type="text" name="name">
                </td>
            </tr>
            <tr>
                <td>სტუდენტის გვარი</td>
                <td>
                    <input type="text" name="lastname">
                </td>
            </tr>
            <tr>
                <td>ლექტორი</td>
                <td>
                    <input type="text" name="lecturer">
                </td>
            </tr>
            <tr>
                <td>დეკანი</td>
                <td>
                    <input type="text" name="dean">
                </td>
            </tr>
            <tr>
                <td>სასწავლო კურსი</td>
                <td>
                    <input type="number" name="course">
                </td>
            </tr>
            <tr>
                <td>სემესტრი</td>
                <td>
                    <input type="number" name="semester">
                </td>
            </tr>
            <tr>
                <td>მიღებული ნიშანი</td>
                <td>
                    <input type="number" name="mark">
                </td>
            </tr> 
            <tr>
                <td>შეფასება</td>
                <td>
                    <input class="inputt" readonly="readonly" type="number" name="grade">
                </td>
            </tr> 
        </table>
        <br>
        <button name="calculate">SUBMIT</button>
        <br><br>
        <a href="form.php">განახლება</a>
        <br><br>
    </form>
    <hr>
    <br><br>
    <?php
    if (isset($_POST['calculate']))
    { 
    ?>
    <table class="table2">
        <tr>
            <th>სახელი</th>
            <th>გვარი</th>
            <th>ლექტორი</th>
            <th>დეკანი</th>
            <th>კურსი</th>
            <th>სემესტრი</th>
            <th>მიღებული ნიშანი</th>
            <th>შეფასება</th>
        </tr>
        <tr>
            <td><?=$_POST['name']?></td>
            <td><?=$_POST['lastname']?></td>
            <td><?=$_POST['lecturer']?></td>
            <td><?=$_POST['dean']?></td>
            <td><?=$_POST['course']?></td>
            <td><?=$_POST['semester']?></td>
            <td><?=$_POST['mark']?></td>
            <td>
                <?=
                $mark = $_POST['grade'];
                if($mark<50 && $mark>=0){
                    echo "(F) ჩაიჭრა";
                }else if($mark<60 && $mark>=50){
                    echo "(E) საკმარისი";
                }else if($mark<70 && $mark>=60){
                    echo "(D) დამაკმაყოფილებელი";
                }else if($mark<80 && $mark>=70){
                    echo "(C) კარგი";
                }else if($mark<90 && $mark>=80){
                    echo "(B) ძალიან კარგი";
                }else if($mark<101 && $mark>=90){
                    echo "(A) ფრიადი";
                }else{
                    echo "არასწორი ქულა! [0-100]";
                }
                ?>
            </td>
        </tr>
    </table>
    <?php
    }
    ?>
</body>
</html>