<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Davaleba_1-1</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>სახელფასო უწყისი</h1>
    <p class="p1"><i>Task_1</i></p>
    <form action="">
        <table class="table1">
            <tr>
                <td>სახელი</td>
                <td>
                    <input type="text" name="name">
                </td>
            </tr>
            <tr>
                <td>გვარი</td>
                <td>
                    <input type="text" name="lastname">
                </td>
            </tr>
            <tr>
                <td>დაკავებული თანამდებობა</td>
                <td>
                    <input type="text" name="position">
                </td>
            </tr>
            <tr>
                <td>ხელფასის რაოდენობა</td>
                <td>
                    <input type="text" onkeyup=calculateTax() name="salary">
                </td>
            </tr>
            <tr>
                <td>დაკავებული საშემოსავლო <i>(%)</i></td>
                <td>
                    <input type="number" name="tax" value="20">
                </td>
            </tr>
        </table>
        <br>
        <button name="calculate">ხელფასის გამოთვლა</button>
        <br><br>
        <a href="form.php"><i>განახლება</i></a>
        <br><br>
    </form>
    <hr>
    <br><br>
    <?php
    if (isset($_GET['calculate']))
    {
        $sashemosavlo = $_GET['salary'] * $_GET['tax']/100;
        $daricxuli = $_GET['salary'] - $sashemosavlo;
    ?>
    <table class="table2">
        <tr>
            <th>სახელი</th>
            <th>გვარი</th>
            <th>დაკავებული თანამდებობა</th>
            <th>ხელფასის რაოდენობა</th>
            <th>დაკავებული საშემოსავლო</th>
            <th>დარიცხული ხელფასი</th>
        </tr>
        <tr>
            <td><?=$_GET['name']?></td>
            <td><?=$_GET['lastname']?></td>
            <td><?=$_GET['position']?></td>
            <td><?=$_GET['salary']?></td>
            <td><?=$sashemosavlo?></td>
            <td><?=$daricxuli?></td>
        </tr>
    </table>
    <?php
    }
    ?>
</body>
</html>