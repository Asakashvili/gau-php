<!-- მონაცემთა ბაზასთან დაკავშირება -->
<?php
$host = "localhost";
$user = "root"; // 'root' არის მომხმარებელი.
$pass = "";
$db = "cars";

$connection = mysqli_connect($host, $user, $pass, $db);
// echo "<pre>";
// print_r($connection);
// echo "</pre>";

// შევამოწმოთ, თუ რაიმე კავშირი არ არის გამართული, მაშინ ქვემოთ შეწყდეს ყველანაირი პროცესი.
if(!$connection){
    die("Database connection Error!!!");
}
?>