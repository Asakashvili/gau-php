<h1>INSERT</h1>
<form action="" method="post">
    <!-- რაც ბაზაში გვიწერია მაგ სახელებს ვწერთ. -->
    <input type="text" class="input" name="name" placeholder="Insert Name..."> 
    <br>
    <input type="text" class="input" name="model" placeholder="Insert Model...">
    <br>
    <input type="text" class="input" name="color" placeholder="Insert Color...">
    <br>
    <input type="date" class="input" name="year" placeholder="Insert Year...">
    <br>
    <input type="number" class="input" name="mileage" placeholder="Insert Mileage...">
    <br>
    <input type="number" class="input" name="price" placeholder="Insert Price...">
    <br>
    <input type="submit" class="inputbutton" value="SUBMIT" name="insert">
</form>

<?php
if(isset($_POST['insert'])){ // თუ არსებობს, თუ დაჭერილია ღილაკზე.
    $name = $_POST["name"];
    $model = $_POST["model"];
    $color = $_POST["color"];
    $year = $_POST["year"];
    $mileage = $_POST["mileage"];
    $price = $_POST["price"];

    $query = "INSERT INTO cars(name, model, color, year, mileage, price) 
              VALUES('$name', '$model', '$color', '$year', '$mileage', '$price')";      
    //  ვამოწმებთ მონაცემები ჩაიწერა თუ არა.
    if(mysqli_query($connection, $query)){
        echo "Record has been successfully added!";
        header("location: index.php?menu=select"); // ლინკისავითაა, ამის მეშვეობით გვერდის დარეფრეშების შემდგომ მონაცემები ავტომატურად აღარ ჩაიწერება.
    }else{
        echo "Error!!";
    }
}
?>