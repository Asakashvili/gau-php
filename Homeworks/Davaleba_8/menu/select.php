<h1>MODIFY</h1>
<table class="datatable">
<!-- '<tbody>' ტეგი გამოიყენება 'HTML' ცხრილის ძირითადი შინაარსის დასაჯგუფებლად. -->
<tbody>
    <!-- '<thead>' 'HTML' ელემენტი განსაზღვრავს სტრიქონების ერთობლიობას, რომელიც განსაზღვრავს ცხრილის სვეტების თავს. -->
    <thead>
        <tr>
            <th>Name</th>
            <th>Model</th>
            <th>Color</th>
            <th>Year</th>
            <th>Mileage</th>
            <th>Price</th>
            <th>EDIT</th>
            <th>DELETE</th>
        </tr>
    </thead>
<!-- 'Connection'-ის გამოყენება -->
<?php
if(isset($_GET["change"]) && $_GET["change"] == "delete"){
    $id = $_GET["id"];
    $delete = "DELETE FROM cars WHERE id=$id";
    mysqli_query($connection, $delete);
}

$select = "SELECT * FROM cars ORDER BY id DESC"; // გამოიტანს მხოლოდ ორ სახელს და გვარს.
// 'ORDER BY id DESC'-ით ჯერ ახალი მონაცემები გამოვა.
$result = mysqli_query($connection, $select); // პირველს ვწერთ თუ რომელ 'connection'-ზე გვინდა და შემდეგ შესაბამისი მოთხოვნა.
// var_dump($result); // გვიბრუნებს რესურს ტიპის მნიშვნელობას, გამოაქვს ძირითადი ინფორმაცია იმ ცხრილის შესახებ რომელზეც 'select' გავაკეთეთ.
// echo mysqli_num_rows($result); // ეს გვიბრუნებს თუ რამდენი ჩანაწერი არის მონიშნული ჩვენი მოთხოვნის შესაბამისად.
if(mysqli_num_rows($result)>0){
    // გამოვიყენეთ 'whil loop' ოპერატორი კონკრეტული ჩანაწერის პირდაპირ გამოსატანად.
    while($row = mysqli_fetch_assoc($result)){ // '_assoc' ანუ გამოვიტანთ ასოციაციური მასივით რომელიც პრაქტიკაში ყველაზე ხშირად გამოიყენება.
        ?>
        <tr>
            <td><?=$row['Name']?></td>
            <td><?=$row['Model']?></td>
            <td><?=$row['Color']?></td>
            <td><?=$row['Year']?></td>
            <td><?=$row['Mileage']?></td>
            <td><?=$row['Price']?></td>
            <td><a style="font-weight: bold; color: white; text-decoration: none;" href="?change=edit&&id=<?=$row['id']?>">EDIT</a></td>
            <td><a style="font-weight: bold; color: white; text-decoration: none;" href="?menu=select&&change=delete&&id=<?=$row['id']?>">DELETE</a></td>
        </tr>
    <?php
    }}
    ?>
    </tbody>
    </table>