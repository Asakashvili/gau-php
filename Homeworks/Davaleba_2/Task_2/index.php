<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task_2</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Task_2</h1>
    <p>4x4 number matrix</p>
    <?php
        // განვსაზღვრე '4x4' რიცხვითი მატრიცა და ელემენტებს მივანიჭე რიცხვები [10; 100] შუალედში.
        $arr = array(array(11, 23, 32, 47), array(51, 26, 67, 88), array(12,97,43,55), array(22, 68, 56, 34));
        echo "<table cellspacing=0>"; // <== გამოიყენება უჯრებს შორის სივრცის დასადგენად. აუცილებელია ცხრილის გამოსატანად. / გავხსენი 'table'.
        for($col=0; $col<4; $col++){ // <== აქ 'loop' მეთოდით მივუთიტე სვეტების რაოდენობა, ანუ '4'.
            echo "<tr>";
            for($row=0; $row<4; $row++){ // <== აქ 'loop' მეთოდით მივუთითე რიგების რაოდენობა, ანუ '4'.
                echo "<td>";
                echo $arr[$col][$row]; // <== 'td'-ებში შევიტანე ზემოთ დაწერილი რიცხვები.
                echo "</td>";
            }
            echo "</tr>";
        }
        echo "</table><br>"; // ცხრილის დახურვა.
    ?>
    
    <!-- ავაგე 'X' რიცხვის შესატანი ფორმა '$_POST' მეთოდის საშუალებით. -->
    <form action="" method="post">
        შეიტანეთ X რიცხვი - <input type="number" name="input" required>
        <br><br>
        <input type="submit" name="submit" class="button" value="Submit">
    </form>

    <?php
    // განვსაზღვრე ცვლადები
    $multiple= array(); // <== ჯერადები.
    $sum=0; // <== ჯამი.
    $multi=1; // <== ნამრავლი. 

    // შევასრულე ოპერაციები.
    if(isset($_POST['submit'])){
        for($col=0; $col<4; $col++){
            for($row=0; $row<4; $row++){
                if($arr[$col][$row]%$_POST['input']==0){
                    array_push($multiple, $arr[$col][$row]); // ერთი ან მეტი ელემენტის შეტანა მასივის ბოლოში.
                }
                $sum += $arr[$col][$row]; // მატრიცის ელემენტების ჯამი.
                $multi *= $arr[$col][$row]; // მატრიცის ელემენტების ნამრავლი.
            }
        }
        $average = $sum/16; // <== რადგან '4x4' ცხრილია.

        // გამოვიტანე ამოცანაში მოცემული პასუხები.
        echo "<br>";
        echo "<b>მატრიცაში არსებული <i>'X'</i> რიცხვის ჯერადი რიცხვები:</b> ";
        print_r($multiple); 
        echo "<br>";
        echo "<b>მატრიცის ელემენტების ჯამი:</b> ".$sum;
        echo "<br>";
        echo "<b>მატრიცის ელემენტების ნამრავლი:</b> ".$multi;
        echo "<br>";
        echo "<b>მატრიცის საშუალო არითმეტიკული:</b> ".$average;
    }
    ?> 
</body>
</html>