<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task_4</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Task_4</h1>
    <?php
    // დავწერე 'header' ცვლადი და შევიტანე სათაურები, შეტანილი ელემენტები იქნება ცხრილის თავში.
    $header = array("Make", "Model", "Color", "Mileage", "Status");
    
    // შემოვიტანე ამოცანაში მოცემული მრავალგანზომილებიანი ასოციაციური მასივი.
    $cars = array(
        array("Make"=>"Toyota",
              "Model"=>"Corolla",
              "Color"=>"White",
              "Mileage"=>24000,
              "Status"=>"Sold"),
        
        array("Make"=>"Toyota",
              "Model"=>"Camry",
              "Color"=>"Black",
              "Mileage"=>56000,
              "Status"=>"Available"),
        
        array("Make"=>"Honda",
              "Model"=>"Accord",
              "Color"=>"White",
              "Mileage"=>15000,
              "Status"=>"Sold")    );
              echo "<br>";

              // მატრიცა გამოვიტანე ცხრილის სახით.
              echo "<table>"; // გავხსენი ცხრილი.
              for($col=0; $col<4; $col++){
                  echo "<tr>";
                  for($row=0; $row<4; $row++){
                      echo "<td>";
                      if($col==0){
                          echo $header[$row];
                      }else{
                        echo $cars[$col-1][$header[$row]];
                  }
                  echo "</td>";
              }
              echo "</tr>";
            }
            echo "</table>"; // დავხურე ცხრილი.
    ?>
</body>
</html>