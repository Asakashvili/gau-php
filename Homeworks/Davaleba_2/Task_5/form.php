<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task_5</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
    // <!-- განვსაზღვრე ცვლადები და ყველას ერთად მივანიჭე სიცარიელე. -->
    $nameError = $emailError = $websiteError = $genderError = "";
    $name = $email = $website = $comment = $gender = "";

    // '$_SERVER' არის მასივი, რომელიც შეიცავს ინფორმაციას, როგორიცაა სათაურები, ბილიკები და სკრიპტის მდებარეობა. 
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(empty($_POST["name"])){ // "empty" მეშვეობით ვეუბნებით არის თუ არა კონკრეტული ცვლადი ცარიელი.
        $nameError = "Name is required"; // თუ არის დაწერს რომ სახელის შეყვანა აუცილბელია.
    }else{ // წინააღმდეგ შემთხვევაში ჩათვლის ჩაწერილად.
        $name = test_input($_POST["name"]); // 'test_input'-ის მეშვეობით ვაწვდით მნიშვნელობას.
        // ვამოწმებთ თუ სახელი შეიცავს მხოლოდ სიმბოლოებს და სფეისს.
        if(!preg_match("/^[a-zA-Z-' ]*$/", $name)){ // 'preg_match'-ს ჩავუწერეთ სიმბოლოები, თუ კონკრეტული სიმბოლო იქნება ჩაწერილი გამოიტანს ქვემოთ მოცემულ ტექსტს.
            $nameError = "Only letters and white space allowed"; // 'Error' ცვლადი ცალკეა განსაზღვრული.
        }
    }

    if(empty($_POST["email"])){
        $emailError = "E-mail is required";
    }else{
        $email = test_input($_POST["email"]);
        // ვამოწმებთ თუ იმეილი სწორადაა შეყვანილი.
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)){ // ფილტრავს ცვლადს მითითებული ფილტრით / 'FILTER_VALIDATE_EMAIL' ამოწმებს ელფოსტის მისამართს.
            $emailError = "Invalid e-mail format";
        }
    }

    if(empty($_POST["website"])){
        $website = "";
    }else{
        $website = test_input($_POST["website"]);
        // ვამოწმებთ არის თუ არა 'URL' მისამართის სინტაქსი სწორი.
        if(!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $website)){
            $websiteError = "Invalid URL";
        }
    }
     
    if(empty($_POST["comment"])){
        $comment = "";
    }else{
        $comment = test_input($_POST["comment"]);
    }

    if(empty($_POST["gender"])){
        $genderError = "Gender is required";
    }else{
        $gender = test_input($_POST["gender"]);
    }
}

    function test_input($data) { // defined test_input; 
        $data = trim($data); // 'trim' ფუნქცია შლის თეთრ სივრცეს და სხვა წინასწარ განსაზღვრულ სიმბოლოებს სტრიქონის ორივე მხრიდან. 
        $data = stripslashes($data); // ეს ფუნქცია შეიძლება გამოყენებულ იქნას მონაცემთა ბაზიდან ან HTML ფორმიდან ამოღებული მონაცემების გასასუფთავებლად. 
        $data = htmlspecialchars($data); // htmlspecialchars() ფუნქცია გარდაქმნის ზოგიერთ წინასწარ განსაზღვრულ სიმბოლოს HTML ერთეულებად. 
        return $data; // დავაბრუნე 'data' ცვლადი.
      }
    ?>

    <form action="" method="post">
    <h2>PHP Form Validation Example</h2>
    <p class="error">* required field</p>
        <br>
        <!-- 'value'-ში ჩავუწერე '$name' ცვლადი რაც ღილაკზე დაჭერის შემდეგ 'input' ველში დატოვებს ცვლადის მნიშვნელობას (ჩვენს მიერ მიწოდებულ ტექსტს). -->
        Name: <input type="text" name="name" value="<?php echo $name;?>"> 
        <!-- აქ არასწორი პარამეტრების შეყვანის შემთხვევაში ჩავუწერე '$nameError' ცვლადი. -->
        <span class="error">* <?php echo $nameError;?></span> 
        <br><br>
        E-Mail: <input type="text" name="email" value="<?php echo $email;?>">
        <span class="error">* <?php echo $emailError;?></span>
        <br><br>
        Website: <input type="text" name="website" value="<?php echo $website;?>"> 
        <span class="error"><?php echo $websiteError;?></span>
        <br><br>
        Comment: <textarea name="comment" id="" cols="50" rows="7"><?php echo $comment;?></textarea>
        <br><br>
        Gender:
        <!-- 'radio'-სთვის ერთიდაიმავე სახელის მითითების შემთხვევაში მათგან მხოლოდ ერთის მონიშვნას შევძლებთ. -->
        <input type="radio" name="gender" <?php if(isset($gender) && $gender=="female") echo "checked";?> value="female">Female
        <input type="radio" name="gender" <?php if(isset($gender) && $gender=="male") echo "checked";?> value="male">Male
        <input type="radio" name="gender" <?php if(isset($gender) && $gender=="other") echo "checked";?> value="other">Other
        <span class="error">* <?php echo $genderError;?></span>
        <br><br>
        <input type="submit" name="submit" value="Submit"> 
        
        <h2>Your Input:</h2>
        <?php
            echo $name;
            echo "<br>";
            echo $email;
            echo "<br>";
            echo $website;
            echo "<br>";
            echo $comment;
            echo "<br>";
            echo $gender;
        ?>
    </form>
</body>
</html>