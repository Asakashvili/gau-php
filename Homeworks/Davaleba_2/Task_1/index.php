<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task_1</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Task_1</h1>
    <form action="" method="post">
        <!-- 'x' - რიცხვის შეტანა მოვახდინე '$_POST' მეთოდის საშვალებით. -->
        <input type="number" name="x">
        <br><br>
        <button name="check">CHECK</button>
        <br><br>
    </form>
    <?php
        $minimum = 0;
        $maximum = 0;
        $arr_numbers = [13, 10, 71, 18, 93, 17, 32, 44, 54, 12, 85, 27]; // მნიშვნელობები 10-დან 100-ის შუალედში.
        echo "<b>Masivi: </b>";
        print_r($arr_numbers); // ცვლადში მოთავსებული ინფორმაციის დაბეჭდვა.
        if(isset($_POST["check"])){
            for($i=0; $i<count($arr_numbers); $i++){
                if ($arr_numbers[$i] < $_POST["x"]){
                    $minimum++;
                }
                else if($arr_numbers[$i] > $_POST["x"]){
                    $maximum++;
                }
            }
            echo "<br><br><b> Masivshi ".$_POST["x"]."-ze didi ".$maximum." ricxvia da patara ".$minimum."</b>";
        }
    ?>
</body>
</html>