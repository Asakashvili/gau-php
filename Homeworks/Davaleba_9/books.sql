-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2022 at 05:47 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `books`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(10) UNSIGNED NOT NULL,
  `Title` varchar(72) NOT NULL,
  `Releasedate` varchar(50) NOT NULL,
  `Author` varchar(48) NOT NULL,
  `Pages` int(11) NOT NULL,
  `Publisher` varchar(48) NOT NULL,
  `Editor` varchar(48) NOT NULL,
  `Designer` varchar(48) NOT NULL,
  `Description` varchar(200) NOT NULL,
  `Views` int(11) NOT NULL,
  `Rating` float NOT NULL,
  `Genre` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `Title`, `Releasedate`, `Author`, `Pages`, `Publisher`, `Editor`, `Designer`, `Description`, `Views`, `Rating`, `Genre`) VALUES
(46, 'uygki', '2022', 'safag', 555, 'safg', 'asgf', 'asggg', '', 2, 3, '333'),
(53, 'Harry Potter', '2001', 'J. K. Rowling', 223, 'Bloomsbury', 'Richard Francis-Bruce', 'Nicolas Flamel', 'Fantasy Novel', 1000000, 7.6, 'Fantasy');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
