<?php
ob_start();
if(isset($_GET["id"])){
    $id = $_GET["id"];
    $select_row = "SELECT * FROM books WHERE id = '$id'";
    $result = mysqli_query($connection, $select_row);
    if(mysqli_num_rows($result)>0){
        $row = mysqli_fetch_assoc($result);
    }
    // echo $row['name'];
}
?>

<h1>EDIT</h1>
<form action="" method="post">
    <!-- რაც ბაზაში გვიწერია მაგ სახელებს ვწერთ. -->
    <input type="text" class="input" name="title" value="<?=$row['Title']?>" placeholder="Edit Title..."> 
    <br><br>
    <label for="releasedate"><b><i>Release Date:</i></b></label>
    <br>
    
    <!-- თარიღის შექმნა წელი, თვე, რიცხვი-ის შესაბამისად 'select' 'option' თაგის გამოყენებით -->
    <select name="releasedate">
        <?php 
          $start_date = 1;
          $end_date = 31;
          for( $i=$start_date; $i<=$end_date; $i++ ) {
            echo '<option value='.$i.'>'.$i.'</option>';
          }
        ?>
      </select>
    
      <select name="releasedate">
        <?php for( $j=1; $j<=12; ++$j ) { 
          $month_label = date('F', mktime(0, 0, 0, $j, 1));
        ?>
          <option value="<?php echo $month_label; ?>"><?php echo $month_label; ?></option>
        <?php } ?>
      </select> 
    
      <select name="releasedate">
        <?php 
          $year = date('Y');
          $min = $year - 60;
          $max = $year;
          for( $k=$max; $k>=$min; $k-- ) {
            echo '<option value='.$k.'>'.$k.'</option>';
          }
        ?>
    </select>
    
    <br><br>
    <input type="text" class="input" name="author" value="<?=$row['Author']?>" placeholder="Edit Author...">
    <br>
    <input type="number" class="input" name="pages" value="<?=$row['Pages']?>" placeholder="Edit the Amount of Pages...">
    <br>
    <input type="text" class="input" name="publisher" value="<?=$row['Publisher']?>" placeholder="Edit Publisher...">
    <br>
    <input type="text" class="input" name="editor" value="<?=$row['Editor']?>" placeholder="Edit Editor...">
    <br>
    <input type="text" class="input" name="designer" value="<?=$row['Designer']?>" placeholder="Edit Designer...">
    <br><br>
    <textarea class="textarea" name="description" placeholder="Edit Short Description..." cols="60" rows="10"><?=$row['Description']?></textarea>
    <br>
    <input type="number" class="input" name="views" value="<?=$row['Views']?>" placeholder="Edit the Amount of Views...">
    <br>
    <input type="text" class="input" name="rating" value="<?=$row['Rating']?>" placeholder="Edit Rating...">
    <br>
    <input type="text" class="input" name="genre" value="<?=$row['Genre']?>" placeholder="Edit Genre...">
    <br>
    <input type="submit" class="inputbutton" value="UPDATE" name="update">
</form>

<?php
if(isset($_POST['update'])){ // თუ არსებობს, თუ დაჭერილია ღილაკზე.
    $title = $_POST["title"];
    $releasedate = $_POST["releasedate"];
    $author = $_POST["author"];
    $pages = $_POST["pages"];
    $publisher = $_POST["publisher"];
    $editor = $_POST["editor"];
    $designer = $_POST["designer"];
    $description = $_POST["description"];
    $views = $_POST["views"];
    $rating = $_POST["rating"];
    $genre = $_POST["genre"];

    $query = "UPDATE books SET title = '$title', releasedate = '$releasedate', author = '$author', pages = '$pages', publisher = '$publisher', editor = '$editor', designer = '$designer',
    description = '$description', views = '$views', rating = '$rating', genre = '$genre' WHERE id='$id'";           
    //  ვამოწმებთ მონაცემები ჩაიწერა თუ არა.
    if(mysqli_query($connection, $query)){
        echo "Record has been successfully updated!";
        header("location: index.php?menu=select"); // ლინკისავითაა, ამის მეშვეობით გვერდის დარეფრეშების შემდგომ მონაცემები ავტომატურად აღარ ჩაიწერება.
      }else{
        echo "Error!!";
    }
}
?>