<h1>INSERT</h1>
<form action="" method="post">
    <!-- რაც ბაზაში გვიწერია მაგ სახელებს ვწერთ. -->
    <input type="text" class="input" name="title" placeholder="Insert Title..."> 
    <br><br>
    <label for="releasedate"><b><i>Release Date:</i></b></label>
    <br>
    
    <!-- თარიღის შექმნა წელი, თვე, რიცხვი-ის შესაბამისად 'select' 'option' თაგის გამოყენებით -->
    <select name="releasedate">
        <!-- days -->
        <?php 
          $start_date = 1;
          $end_date = 31;
          for( $i=$start_date; $i<=$end_date; $i++ ) {
            echo '<option value='.$i.'>'.$i.'</option>';
          }
        ?>
      </select>
    
      <!-- months -->
      <select name="releasedate">
        <?php for( $j=1; $j<=12; ++$j ) { 
          $month_label = date('F', mktime(0, 0, 0, $j, 1));
        ?>
          <option value="<?php echo $month_label; ?>"><?php echo $month_label; ?></option>
        <?php } ?>
      </select> 
    
      <!-- years -->
      <select name="releasedate">
        <?php 
          $year = date('Y');
          $min = $year - 60;
          $max = $year;
          for( $k=$max; $k>=$min; $k-- ) {
            echo '<option value='.$k.'>'.$k.'</option>';
          }
        ?>
    </select>
    
    <br><br>
    <input type="text" class="input" name="author" placeholder="Insert Author...">
    <br>
    <input type="number" class="input" name="pages" placeholder="Insert the Amount of Pages...">
    <br>
    <input type="text" class="input" name="publisher" placeholder="Insert Publisher...">
    <br>
    <input type="text" class="input" name="editor" placeholder="Insert Editor...">
    <br>
    <input type="text" class="input" name="designer" placeholder="Insert Designer...">
    <br><br>
    <textarea class="textarea" name="description" placeholder="Insert Short Description..." cols="60" rows="10"></textarea>
    <br>
    <input type="number" class="input" name="views" placeholder="Insert the Amount of Views...">
    <br>
    <input type="text" class="input" name="rating" placeholder="Insert Rating...">
    <br>
    <input type="text" class="input" name="genre" placeholder="Insert Genre...">
    <br>
    <input type="submit" class="inputbutton" value="INSERT" name="insert">
</form>

<?php
if(isset($_POST['insert'])){ // თუ არსებობს, თუ დაჭერილია ღილაკზე.
    $title = $_POST["title"];
    $releasedate = $_POST["releasedate"];
    $author = $_POST["author"];
    $pages = $_POST["pages"];
    $publisher = $_POST["publisher"];
    $editor = $_POST["editor"];
    $designer = $_POST["designer"];
    $description = $_POST["description"];
    $views = $_POST["views"];
    $rating = $_POST["rating"];
    $genre = $_POST["genre"];

    $query = "INSERT INTO books(title, releasedate, author, pages, publisher, editor, designer, description, views, rating, genre) 
              VALUES('$title', '$releasedate', '$author', '$pages', '$publisher', '$editor', '$designer', '$description', '$views', '$rating', '$genre')";      
    //  ვამოწმებთ მონაცემები ჩაიწერა თუ არა.
    if(mysqli_query($connection, $query)){
        echo "Record has been successfully added!";
        header("location: index.php?menu=select"); // ლინკისავითაა, ამის მეშვეობით გვერდის დარეფრეშების შემდგომ მონაცემები ავტომატურად აღარ ჩაიწერება.
    }else{
        echo "Error!!";
    }
}
?>