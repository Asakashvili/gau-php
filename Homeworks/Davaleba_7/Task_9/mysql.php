<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MySQL - Task_9</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <!-- სათაურები -->
    <h1>PHP - MySQL</h1>
    <h2>Task_9</h2>

    <!-- აქედან ფაილი დავუკავშირე ბაზას. -->
    <?php
    $connection = mysqli_connect('localhost','root','','mydata');
    if(!$connection){
        die("Database connection Error!!!");
    }
    ?>

    <!-- 'users' ცხრილიდან გამოვიტანე პირველი 3 ჩანაწერის 'Age, Date, Reg_Date, Gender' ველების მნიშვნელობები. -->
    <?php
    $select = "SELECT Age, Date, Reg_Date, Gender FROM users WHERE id<=3";
    $result = mysqli_query($connection, $select);
    if(!$result){
        die("Error!");
    }

    echo "1. display Age, Date, Reg_Date and Gender of the first three records from the users table";
    echo "<div class='container'>";
    foreach($result as $resultt){
        echo $resultt['Age']."&nbsp&nbsp&nbsp&nbsp".$resultt['Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Reg_Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Gender']."<br>";
    }
    echo "</div>";
    ?>

    <br>

    <!-- 'users' ცხრილიდან გამოვიტანე პირველი ორი ჩანაწერის ყველა ველის მნიშვნელობები. -->
    <?php
    $select = "SELECT id, Name, Lastname, Age, Date, Reg_Date, Password, Gender FROM users WHERE id<3";
    $result = mysqli_query($connection, $select);
    if(!$result){
        die("Error!");
    }

    echo "2. display all the values from the first 2 records from users table";
    echo "<div class='container'>";
    foreach($result as $resultt){
        echo $resultt['id']."&nbsp&nbsp&nbsp&nbsp".$resultt['Name']."&nbsp&nbsp&nbsp&nbsp".$resultt['Lastname']."&nbsp&nbsp&nbsp&nbsp".$resultt['Age']."&nbsp&nbsp&nbsp&nbsp".$resultt['Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Reg_Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Gender']."<br>";
    }
    echo "</div>";
    ?>

    <br>

    <!-- 'users' ცხრილიდან გამოვიტანე იმ ჩანაწერების ყველა ველის მნიშვნელობები რომელთა 'id>1' და 'id<4'. -->
    <?php
    $select = "SELECT id, Name, Lastname, Age, Date, Reg_Date, Password, Gender FROM users WHERE id>1 AND id<4";
    $result = mysqli_query($connection, $select);
    if(!$result){
        die("Error!");
    }

    echo "3. display all the record values from users table where id is greater than 1 and less than 4";
    echo "<div class='container'>";
    foreach($result as $resultt){
        echo $resultt['id']."&nbsp&nbsp&nbsp&nbsp".$resultt['Name']."&nbsp&nbsp&nbsp&nbsp".$resultt['Lastname']."&nbsp&nbsp&nbsp&nbsp".$resultt['Age']."&nbsp&nbsp&nbsp&nbsp".$resultt['Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Reg_Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Gender']."<br>";
    }
    echo "</div>";
    ?>

    <br>

    <!-- 'users' ცხრილიდან გამოვიტანე იმ ჩანაწერების ველა ველის მნიშვნელობები რომელთა 'id<2' ან 'id>=4'; -->
    <?php
    $select = "SELECT id, Name, Lastname, Age, Date, Reg_Date, Password, Gender FROM users WHERE id<2 OR id>=4";
    $result = mysqli_query($connection, $select);
    if(!$result){
        die("Error!");
    }

    echo "4. display all the record values from users table where id is less than 2 or greater or equal than 4";
    echo "<div class='container'>";
    foreach($result as $resultt){
        echo $resultt['id']."&nbsp&nbsp&nbsp&nbsp".$resultt['Name']."&nbsp&nbsp&nbsp&nbsp".$resultt['Lastname']."&nbsp&nbsp&nbsp&nbsp".$resultt['Age']."&nbsp&nbsp&nbsp&nbsp".$resultt['Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Reg_Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Gender']."<br>";
    }
    echo "</div>";
    ?>

    <br>

    <!-- 'users' ცხრილიდან გამოვიტანე იმ ჩანაწერის ყველა ველის მნიშვნელობები რომლის 'Date=2014-07-04'. -->
    <?php
    $select = "SELECT id, Name, Lastname, Age, Date, Reg_Date, Password, Gender FROM users WHERE Date='2014-07-04'";
    $result = mysqli_query($connection, $select);
    if(!$result){
        die("Error!");
    }

    echo "5. display all the record values from users table where Date is equal to 2014-07-04";
    echo "<div class='container'>";
    foreach($result as $resultt){
        echo $resultt['id']."&nbsp&nbsp&nbsp&nbsp".$resultt['Name']."&nbsp&nbsp&nbsp&nbsp".$resultt['Lastname']."&nbsp&nbsp&nbsp&nbsp".$resultt['Age']."&nbsp&nbsp&nbsp&nbsp".$resultt['Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Reg_Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Gender']."<br>";
    }
    echo "</div>";
    ?>

    <br>

    <!-- 'users' ცხრილიდან გამოვიტანე იმ ჩანაწერის ყველა ველის მნიშვნელობები რომელთა 'Date>2014-07-04'. -->
    <?php
    $select = "SELECT id, Name, Lastname, Age, Date, Reg_Date, Password, Gender FROM users WHERE Date>'2014-07-04'";
    $result = mysqli_query($connection, $select);
    if(!$result){
        die("Error!");
    }

    echo "6. display all the record values from users table where Date is greater than 2014-07-04";
    echo "<div class='container'>";
    foreach($result as $resultt){
        echo $resultt['id']."&nbsp&nbsp&nbsp&nbsp".$resultt['Name']."&nbsp&nbsp&nbsp&nbsp".$resultt['Lastname']."&nbsp&nbsp&nbsp&nbsp".$resultt['Age']."&nbsp&nbsp&nbsp&nbsp".$resultt['Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Reg_Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Gender']."<br>";
    }
    echo "</div>";
    ?>

    <br>

    <!-- 'users' ცხრილიდან გამოვიტანე იმ ჩანაწერის ყველა ველის მნიშვნელობები რომელთა 'Date>2014-06-04' და 'Date<2014-07-04'. -->
    <?php
    $select = "SELECT id, Name, Lastname, Age, Date, Reg_Date, Password, Gender FROM users WHERE Date>'2014-06-04' AND Date<'2014-07-04'";
    $result = mysqli_query($connection, $select);
    if(!$result){
        die("Error!");
    }

    echo "7. display all the record values from users table where Date is greater than 2014-06-04 and less than 2014-07-04";
    echo "<div class='container'>";
    foreach($result as $resultt){
        echo $resultt['id']."&nbsp&nbsp&nbsp&nbsp".$resultt['Name']."&nbsp&nbsp&nbsp&nbsp".$resultt['Lastname']."&nbsp&nbsp&nbsp&nbsp".$resultt['Age']."&nbsp&nbsp&nbsp&nbsp".$resultt['Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Reg_Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Gender']."<br>";
    }
    echo "</div>";
    ?>

<br>

<!-- 'users' ცხრილიდან გამოვიტანე იმ ჩანაწერის ყველა ველის მნიშვნელობები რომელთა 'Age=>10' და 'Age<=18'. -->
<?php
$select = "SELECT id, Name, Lastname, Age, Date, Reg_Date, Password, Gender FROM users WHERE (Age=10 or Age>10) AND (Age<18 or Age=18)";
$result = mysqli_query($connection, $select);
if(!$result){
    die("Error!");
}

echo "8. display all the record values from users table where Age is either equal is or greater than 10 and Age is either less or equal than 18";
echo "<div class='container'>";
foreach($result as $resultt){
    echo $resultt['id']."&nbsp&nbsp&nbsp&nbsp".$resultt['Name']."&nbsp&nbsp&nbsp&nbsp".$resultt['Lastname']."&nbsp&nbsp&nbsp&nbsp".$resultt['Age']."&nbsp&nbsp&nbsp&nbsp".$resultt['Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Reg_Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Gender']."<br>";
}
echo "</div>";
?>
</body>
</html>