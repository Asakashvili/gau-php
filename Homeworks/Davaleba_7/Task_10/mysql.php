<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MySQL - Task_10</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <!-- სათაურები -->
    <h1>PHP - MySQL</h1>
    <h2>Task_10</h2>

    <!-- აქედან ფაილი დავუკავშირე ბაზას. -->
    <?php
    $connection = mysqli_connect('localhost','root','','mydata');
    if(!$connection){
        die("Database connection Error!!!");
    }
    ?>

    <!-- 'data' ცხრილიდან გამოვიტანე იმ ჩანაწერების ყველა ველის მნიშვნელობები რომელთა 'id<=7' და 'type=2'. -->
    <?php
    $select = "SELECT id, Title, Date, Type, Photo, Text, Author, Description, Meta_k, Meta_d FROM data WHERE id<=7 AND Type=2";
    $result = mysqli_query($connection, $select);
    if(!$result){
        die("Error!");
    }

    echo "1. display all the record values from data table where id<=7 and type=2";
    echo "<div class='container'>";
    foreach($result as $resultt){
        echo $resultt['id']."&nbsp&nbsp&nbsp&nbsp".$resultt['Title']."&nbsp&nbsp&nbsp&nbsp".$resultt['Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Type']
        ."&nbsp&nbsp&nbsp&nbsp".$resultt['Photo']."&nbsp&nbsp&nbsp&nbsp".$resultt['Text']."&nbsp&nbsp&nbsp&nbsp".$resultt['Author']
        ."&nbsp&nbsp&nbsp&nbsp".$resultt['Description']."&nbsp&nbsp&nbsp&nbsp".$resultt['Meta_k']."&nbsp&nbsp&nbsp&nbsp".$resultt['Meta_d']."<br>";
    }
    echo "</div>";
    ?>

    <br>

    <!-- 'data' ცხრილიდან გამოვიტანე ბოლო 5 ჩანაწერის ყველა ველის მნიშვნელობები. -->
    <?php
    $select = "SELECT id, Title, Date, Type, Photo, Text, Author, Description, Meta_k, Meta_d FROM data WHERE id>=8";
    $result = mysqli_query($connection, $select);
    if(!$result){
        die("Error!");
    }

    echo "2. display the last 5 record values from data table";
    echo "<div class='container'>";
    foreach($result as $resultt){
        echo $resultt['id']."&nbsp&nbsp&nbsp&nbsp".$resultt['Title']."&nbsp&nbsp&nbsp&nbsp".$resultt['Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Type']
        ."&nbsp&nbsp&nbsp&nbsp".$resultt['Photo']."&nbsp&nbsp&nbsp&nbsp".$resultt['Text']."&nbsp&nbsp&nbsp&nbsp".$resultt['Author']
        ."&nbsp&nbsp&nbsp&nbsp".$resultt['Description']."&nbsp&nbsp&nbsp&nbsp".$resultt['Meta_k']."&nbsp&nbsp&nbsp&nbsp".$resultt['Meta_d']."<br>";
    }
    echo "</div>";
    ?>

    <br>

    <!-- 'data' ცხრილიდან გამოვიტანე ბოლო 10 ჩანაწერის ყველა ველის მნიშვნელობები შებრუნებული რიგით. -->
    <?php
    $select = "SELECT id, Title, Date, Type, Photo, Text, Author, Description, Meta_k, Meta_d FROM data WHERE id>=3 ORDER BY id DESC";
    $result = mysqli_query($connection, $select);
    if(!$result){
        die("Error!");
    }

    echo "3. display the last 10 record values from data table in reverse";
    echo "<div class='container'>";
    foreach($result as $resultt){
        echo $resultt['id']."&nbsp&nbsp&nbsp&nbsp".$resultt['Title']."&nbsp&nbsp&nbsp&nbsp".$resultt['Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Type']
        ."&nbsp&nbsp&nbsp&nbsp".$resultt['Photo']."&nbsp&nbsp&nbsp&nbsp".$resultt['Text']."&nbsp&nbsp&nbsp&nbsp".$resultt['Author']
        ."&nbsp&nbsp&nbsp&nbsp".$resultt['Description']."&nbsp&nbsp&nbsp&nbsp".$resultt['Meta_k']."&nbsp&nbsp&nbsp&nbsp".$resultt['Meta_d']."<br>";
    }
    echo "</div>";
    ?>

    <br>

    <!-- 'data' ცხრილიდან გამოვიტანე ლუწი 'id'-ის მქონე ჩანაწერების ყველა ველის მნიშვნელობები. -->
    <?php
    $select = "SELECT id, Title, Date, Type, Photo, Text, Author, Description, Meta_k, Meta_d FROM data WHERE id%2=0";
    $result = mysqli_query($connection, $select);
    if(!$result){
        die("Error!");
    }

    echo "4. display all the record values from data table where the id is even";
    echo "<div class='container'>";
    foreach($result as $resultt){
        echo $resultt['id']."&nbsp&nbsp&nbsp&nbsp".$resultt['Title']."&nbsp&nbsp&nbsp&nbsp".$resultt['Date']."&nbsp&nbsp&nbsp&nbsp".$resultt['Type']
        ."&nbsp&nbsp&nbsp&nbsp".$resultt['Photo']."&nbsp&nbsp&nbsp&nbsp".$resultt['Text']."&nbsp&nbsp&nbsp&nbsp".$resultt['Author']
        ."&nbsp&nbsp&nbsp&nbsp".$resultt['Description']."&nbsp&nbsp&nbsp&nbsp".$resultt['Meta_k']."&nbsp&nbsp&nbsp&nbsp".$resultt['Meta_d']."<br>";
    }
    echo "</div>";
    ?>
</body>
</html>