-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 25, 2022 at 11:27 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mydata`
--

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE `data` (
  `id` int(10) UNSIGNED NOT NULL,
  `Title` varchar(30) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `Type` varchar(30) DEFAULT NULL,
  `Photo` varchar(50) DEFAULT NULL,
  `Text` text DEFAULT NULL,
  `Author` varchar(30) DEFAULT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `Meta_k` varchar(200) DEFAULT NULL,
  `Meta_d` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`id`, `Title`, `Date`, `Type`, `Photo`, `Text`, `Author`, `Description`, `Meta_k`, `Meta_d`) VALUES
(1, 'Casino Royale', '2006-11-16', 'Action', 'Photo #1', 'Teqsti #1', 'Paul Haggis', 'This is an action movie', 'Movie, Action', 'Casino Royale is an action movie starring Daniel Craig'),
(2, 'Bean', '1997-08-01', 'Comedy', 'Photo #2', 'Teqsti #2', 'Rowan Atkinson', 'This is a comedy movie', 'Movie, Comedy', 'Bean is a comedy movie starring Rowan Atkinson'),
(3, 'The Godfather', '1972-03-24', 'Drama', 'Photo #3', 'Teqsti #3', 'Francis Ford Coppola', 'This is a drama movie', 'Movie, Drama', 'The Godfather is a drama movie starring Marlon Brando'),
(4, 'Narnia', '2005-12-08', 'Fantasy', 'Photo #4', 'Teqsti #4', 'Andrew Adamson', 'This is a fantasy movie', 'Movie, Fantasy', 'Narnia is a fantasy movie starring Anna Popplewell'),
(5, 'The Others', '2001-08-02', '2', 'Photo #5', 'Teqsti #5', 'Alejandro Amenábar', 'This is a horror movie', 'Movie, Horror', 'The Others is a horror movie starring Nicole Kidman'),
(6, 'The Invisible Guest', '2014-01-17', 'Mystery', 'Photo #6', 'Teqsti #6', 'Oriol Paulo', 'This is a mystery movie', 'Movie, Mystery', 'The Invisible Guest is a mystery movie starring Mario Casas'),
(7, 'A Walk To Remember', '2002-01-25', 'Romance', 'Photo #7', 'Teqsti #7', 'Nicholas Sparks', 'This is a romance movie', 'Movie, Romance', 'A Walk To Remember is a romance movie starring Shane West'),
(8, 'Southpaw', '2015-07-20', 'Thriller', 'Photo #8', 'Teqsti #8', 'Kurt Sutter', 'This is a thriller movie', 'Movie, Thriller', 'This is a thriller movie starring Jake Gyllenhaal'),
(9, 'The Revenant', '2015-12-16', 'Western', 'Photo #9', 'Teqsti #9', ' Alejandro González Iñárritu', 'This is a western movie', 'Movie, Western', 'The Revenant is a western movie starring Leonardo DiCaprio'),
(10, 'Jumanji', '2020-08-26', 'Adventure', 'Photo #10', 'Teqsti #10', 'Jim Strain', 'This is an adventure movie', 'Movie, Adventure', 'Jumanji is an adventure movie starring Robin Williams'),
(11, 'Never Back Down', '2008-03-14', 'Sports', 'Photo #11', 'Teqsti #11', 'Chris Hauty', 'This is a sports movie', 'Movie, Sports', 'Never Back Down is a sports movie starring Sean Faris'),
(12, 'The Adam Project', '2021-04-14', 'Sci-Fi', 'Photo #12', 'Teqsti #12', 'Jonathan Tropper', 'This is a sci-fi movie', 'Movie, Sci-Fi', 'The Adam Project is a sci-fi movie starring Ryan Reynolds');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) UNSIGNED NOT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `Meta_k` varchar(200) DEFAULT NULL,
  `Meta_d` varchar(200) DEFAULT NULL,
  `Text` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `Title`, `Meta_k`, `Meta_d`, `Text`) VALUES
(1, 'ფილმები', 'Website, PHP, MySQL', 'This is the first record', 'Teqsti #1'),
(2, 'Satauri #2', 'Website, PHP, MySQL', 'This is the second record', 'Teqsti #2'),
(3, 'თამაშები', 'Website, PHP, MySQL', 'This is the third record', 'Teqsti #3'),
(4, 'Satauri #4', 'Website, PHP, MySQL', 'This is the fourth record', 'Teqsti #4'),
(5, 'მუსიკები', 'Website, PHP, MySQL', 'This is the fifth record', 'Teqsti #5');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `Name` varchar(30) DEFAULT NULL,
  `Lastname` varchar(30) DEFAULT NULL,
  `Age` int(11) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `Reg_Date` date DEFAULT NULL,
  `Password` varchar(50) DEFAULT NULL,
  `Gender` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `Name`, `Lastname`, `Age`, `Date`, `Reg_Date`, `Password`, `Gender`) VALUES
(1, 'Giorgi', 'Vachnadze', 19, '2002-05-03', '2020-05-06', 'giorgivachnadze', 'Male'),
(2, 'Nino', 'Maisuradze', 20, '2014-06-29', '2022-03-09', 'ninomaisuradze', 'Female'),
(3, 'Nika', 'Manelidze', 23, '1999-02-20', '2022-04-27', 'nikamanelidze', 'Male'),
(4, 'Salome', 'Abashidze', 18, '2014-07-04', '2021-11-17', 'salomeabashidze', 'Female'),
(5, 'Ana', 'Gigauri', 24, '2016-12-29', '2021-11-24', 'anagigauri', 'Female');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data`
--
ALTER TABLE `data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
