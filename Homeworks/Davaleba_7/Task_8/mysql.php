<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MySQL - Task_8</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <!-- სათაურები -->
    <h1>PHP - MySQL</h1>
    <h2>Task_8</h2>

    <!-- აქედან ფაილი დავუკავშირე ბაზას. -->
    <?php
    $connection = mysqli_connect('localhost','root','','mydata');
    if(!$connection){
        die("Database Connection Error!!!");
    }
    ?>

    <!-- 'menu' ცხრილიდან გამოვიტანე მხოლოდ პირველი ჩანაწერის 'Text, Title' ველის მნიშვნელობები. -->
    <?php
    $select = "SELECT Text, Title FROM menu WHERE id = 1";
    $result = mysqli_query($connection, $select);
    if(!$result){
        die("Error!");
    }

    echo "1. display the first record text & title row values only from the menu table";
    $row = mysqli_fetch_assoc($result);
    echo "<div class='container'>";
    echo $row['Text'];
    echo "<br>";
    echo $row['Title'];
    echo "</div>";
    ?>

    <br>

    <!-- 'menu' ცხრილიდან გამოვიტანე ყველა ჩანაწერის 'Text, Title' ველის მნიშვნელობები. -->
    <?php
    $select = "SELECT Text, Title FROM menu";
    $result = mysqli_query($connection, $select);
    if(!$result){
        die("Error!");
    }

    echo "2. display all the text & title row values from the menu table";
    echo "<div class='container'>";
    foreach($result as $resultt){
        echo $resultt['Text']."&nbsp&nbsp&nbsp&nbsp".$resultt['Title']."<br>";
    }
    echo "</div>";
    ?>

    <br>

    <!-- 'menu' ცხრილიდან გამოვიტანე იმ ჩანაწერის ყველა ველის მნიშვნელობები რომლის 'id=2'. -->
    <?php
    $select = "SELECT Title, Meta_k, Meta_d, Text FROM menu WHERE id=2";
    $result = mysqli_query($connection, $select);
    $row = mysqli_fetch_assoc($result);

    echo "3. display all the record values of id 2 from the menu table";
    echo "<div class='container'>";
    echo $row['Title']."<br>".$row['Text']."<br>".$row['Meta_k']."<br>".$row['Meta_d']."<br>";
    echo "</div>";
    ?>

    <br>

    <!-- 'menu' ცხრილიდან გამოვიტანე იმ ჩანაწერების ყველა ველის მნიშვნელობები, რომელთა 'id=>2' (ანუ ორის და ზევით). -->
    <?php
    $select = "SELECT Title, Meta_k, Meta_d, Text FROM menu WHERE id=2 OR id>2";
    $result = mysqli_query($connection, $select);
    $row = mysqli_fetch_assoc($result);

    echo "4. display all the record values of id 2 and above from the menu table (id=>2)";
    echo "<div class='container'>";
    foreach($result as $resultt){
        echo $resultt['Text']."&nbsp&nbsp&nbsp&nbsp".$resultt['Title']."&nbsp&nbsp&nbsp&nbsp".$resultt['Meta_k']."&nbsp&nbsp&nbsp&nbsp".$resultt['Meta_d']."<br>";
    }
    echo "</div>";
    ?>

    <br>

    <!-- 'menu' ცხრილიდან გამოვიტანე იმ ჩანაწერების ყველა ველის მნიშვნელობები, რომელთა 'id<=4' (ანუ ოთხის და ქვევით). -->
    <?php
    $select = "SELECT Title, Meta_k, Meta_d, Text FROM menu WHERE id=4 OR id<4";
    $result = mysqli_query($connection, $select);
    $row = mysqli_fetch_assoc($result);

    echo "5. display all the record values of id 4 and below from the menu table (id<=4)";
    echo "<div class='container'>";
    foreach($result as $resultt){
        echo $resultt['Text']."&nbsp&nbsp&nbsp&nbsp".$resultt['Title']."&nbsp&nbsp&nbsp&nbsp".$resultt['Meta_k']."&nbsp&nbsp&nbsp&nbsp".$resultt['Meta_d']."<br>";
    }
    echo "</div>";
    ?>

    <br>

    <!-- 'menu' ცხრილიდან გამოვიტანე იმ ჩანაწერების ყველა ველის მნიშვნელობები, რომელთა "Title='ფილმები' ან Title='თამაშები'". -->
    <?php
    $select = "SELECT id, Title, Meta_k, Meta_d, Text FROM menu WHERE Title='ფილმები' OR Title='თამაშები'";
    $result = mysqli_query($connection, $select);
    $row = mysqli_fetch_assoc($result);

    echo "6. display all the record values of titles that is equal to either 'ფილმები' or 'თამაშები' from the menu table";
    echo "<div class='container'>";
    foreach($result as $resultt){
        echo $resultt['id']."&nbsp&nbsp&nbsp&nbsp".$resultt['Title']."&nbsp&nbsp&nbsp&nbsp".$resultt['Text']."&nbsp&nbsp&nbsp&nbsp".$resultt['Meta_k']."&nbsp&nbsp&nbsp&nbsp".$resultt['Meta_d']."<br>";
    }
    echo "</div>";
    ?>

    <br>

    <!-- 'menu' ცხრილიდან გამოვიტანე იმ ჩანაწერების ყველა ველის მნიშვნელობები, რომელთა "Title='მუსიკები' და 'Id>3'"; -->
    <?php
    $select = "SELECT Title, Meta_k, Meta_d, Text FROM menu WHERE Title='მუსიკები' OR id>3";
    $result = mysqli_query($connection, $select);
    $row = mysqli_fetch_assoc($result);

    echo "7. display all the record values of titles that is equal to 'მუსიკები' and id>3";
    echo "<div class='container'>";
    foreach($result as $resultt){
        echo $resultt['Title']."&nbsp&nbsp&nbsp&nbsp".$resultt['Text']."&nbsp&nbsp&nbsp&nbsp".$resultt['Meta_k']."&nbsp&nbsp&nbsp&nbsp".$resultt['Meta_d']."<br>";
    }
    echo "</div>"
    ?>
</body>
</html>