<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task_3</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Task_3</h1>
    <p>captcha</p>

    <!-- აქ შემოვიტანე ხუთნიშნა შემთხვევითი რიცხვები. -->
    <?php
    $randSymbols = rand(10000, 99999); // განვსაზღვრე ცვლადიც.
    ?>

    <!-- შევადგინე შესაბამისი ფორმა. -->
    <form action="" method="post">
        <div class="div1">
            <!-- 'input'-ში 'value'-დ შევიტანე შემთხვევითი რიცხვები და ეს 'input' გავხადე 'readonly'. -->
            <input name="word" class="word" value="<?=$randSymbols?>" readonly><br>
        </div>
        <br>
        <input type="submit" class="reset" value="Reset">
        <br>
        <input type="text" placeholder="Please insert the numbers..." name="info" class="info">
        <br>
        <input type="submit" name="check" class="check" value="Check">
    </form>
    <br>
    
    <!-- აქ დავწერე თუ რა მოხდება კონკრეტულ ღილაკზე დაჭერის შემთხვევაში / შემოწმება. -->
    <?php
    if(isset($_POST['check'])){
        if($_POST['info']==$_POST['word']){
            echo '<font color="green"><b>თქვენს მიერ შეყვანილი კოდი სწორია!</b></font>';
        }else{
            echo '<font color="red"><b>თქვენს მიერ შეყვანილი კოდი არასწორია!</b></font>';
        }
    }
    ?>
</body>
</html>