<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task_7</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <!-- სათაური -->
    <h1>Task_7</h1>
    <p>vector generator</p>

    <!-- ფორმა -->
    <form action="" method="post">
        <input type="number" name="M" placeholder="Please insert M...">
        <br><br>
        <input type="number" name="a" placeholder="Please insert a...">
        <br><br>
        <input type="number" name="b" placeholder="Please insert b...">
        <br><br>
        <input type="submit" value="Generate" class="generate" name="generate">
        <br><br>
    </form>

    <!-- ღილაკზე დაჭერის შემდეგი გამოსატანი ოპერაციების შედეგი -->
    <?php
    function ops(){ // დავწერე ფუნქცია და დავარქვი სახელი.
    // დავწერე ცვლადები რომლებიც გავუტოლე უარყოფას.
        $valuebool = false;
        $mbool = false;
        $abbool = false;
        if(isset($_POST["generate"])){
            // აქაც განვსაზღვრე 3 ცვლადი და გადავეცი ფორმაში დაწერილი შესაბამისი სახელები.
            $M=$_POST["M"];
            $a=$_POST["a"];
            $b=$_POST["b"];
            
            if(trim($M)!='' && trim($a)!='' && trim($b)!=''){
                $valuebool = true;
            }

            if($valuebool){
                if($M >= 1){
                    $mbool = true;
                }
                if(abs($a-$b)>0){
                    $abbool = true;
                }
            }

            if($mbool && $abbool && $valuebool){
                $numbers = array();
                for($x=0; $x<$M; $x++){
                    $numbers[$x] = rand($a, $b);
                }
                echo "<table>";
                echo "<tr>";
                for($row=0; $row<$M; $row++){
                    echo "<td>";
                    echo $numbers[$row];
                    echo "</td>";
                }
                echo "</table><br>";
            }
            else{
                echo "<font color='red'><b>დაფიქსირდა შეცდომა!</b></font>";
            }
        }
    }
    ops(); // გამოვიძახე ფუნქცია რომ საბოლოოდ ჩაიტვირთოს და იმუშაოს.
    ?>
</body>
</html>