<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task_5</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <!-- სათაურები -->
    <h1>Task_5</h1>
    <p>identify the length of inserted number</p>

    <!-- ფორმა -->
    <form action="" method="post">
        <span>Please insert desired number: </span><input type="number" name="number">
        <br><br>
        <input type="submit" name="submit" class="submit" value="Submit">
    </form>
    <br><br>

    <!-- ღილაკზე დაჭერის შემდეგ => -->
    <?php
    function strlength(){ // დავწერე ფუნქცია და დავარქვი სახელი.
        if(isset($_POST['submit'])){
            echo "თქვენს მიერ შეყვანილი რიცხვი არის <font color='red'<b>".strlen($_POST['number'])."</b></font> ნიშნა.";
            // 'strlen' ითვლის სტრიქონში შემავალი სიმბოლოების ან რიცხვების რაოდენობას.
        }
    }
    strlength(); // გამოვიძახე ფუნქცია რომ საბოლოოდ ჩაიტვირთოს და იმუშაოს.
    ?>
</body>
</html>