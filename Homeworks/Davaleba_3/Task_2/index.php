<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task_2</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <!-- ავაგე რიცხვების შესატანი შესაბამისი ფორმა; -->
    <h1>Task_2</h1>
    <p>table generator</p>
    <form action="" method="post">
        <label for="M">M</label>
        <br>
        <input type="number" name="M">
        <br><br>
        <label for="N">N</label>
        <br>
        <input type="number" name="N">
        <br><br>
        <label for="a">a</label>
        <br>
        <input type="number" name="a">
        <br><br>
        <label for="b">b</label>
        <br>
        <input type="number" name="b">
        <br><br>
        <input type="submit" name="generate" value="Generate">
        <br><br>
    </form>

    <?php
    // 'Generate' ღილაკზე დაჭერის შემდეგ რა მოხდება.
    if(isset($_POST['generate'])){
        // განვსაზღვრე ცვლადები.
        $M=$_POST['M'];
        $N=$_POST['N'];
        $a=$_POST['a'];
        $b=$_POST['b'];

        // შემოვიტანე ცხრილი 'for loop' მეთოდით.
            echo "<table>";
            for($col=0; $col<$N; $col++){
                echo "<tr>";
                for($row=0; $row<$M; $row++){
                    echo "<td>";
                    echo rand ($a, $b); // 'td' თეგში ჩავუწერე '$a' და '$b' ცვლადები, ანუ მინიმალური და მაქსიმალური რიცხვები.
                    echo "</td>";
                }
                echo "</tr>";
            }
            echo "</table><br>";
        }
    ?>
</body>
</html>