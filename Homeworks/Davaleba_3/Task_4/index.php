<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task_4</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

    <h1>Task_4</h1>
    <p>captcha, correct answers</p>
    
    <?php
    $randNumber1 = rand(10, 99); // შემოვიტანე შემთხვევითი ორნიშნა რიცხვები როგორც ამოცანაში იყო ნახსენები.
    $randNumber2 = rand(10, 99);
    $op = array('+', '-'); // მასივის სახით შემოვიტანე შემთხვევითი არითმეტიკული ოპერაციის სიმბოლო (მიმატება, გამოკლება).
    $randop = $op[rand(0, 1)]; // მივანიჭე ინდექსები.
    switch($randop){  
        case '+': // რა ხდება მიმატების შემთხვევაში.
            $result = $randNumber1 + $randNumber2; // რა ხდება მიმატების შემთხვევაში / აქვე შემოვიტანე ახალი ცვლადიც რომელიც ამ ორი რიცხვის ჯამს გააერთიანებს.
            break; // 'break'-ის მეშვეობით ვაჩერებთ მინდინარე კოდის პროცესს.
        case '-': // რა ხდება გამოკლების შემთხვევაში.
            $result = $randNumber1 - $randNumber2; // რა ხდება გამოკლების შემთხვევაში.
            break; // 'break'-ის მეშვეობით ვაჩერებთ მინდინარე კოდის პროცესს.
    }
    $lastresult = $randNumber1 . $randop . $randNumber2; // აქ ასევთქვათ პირველი შემთხვევითი რიცხვი, ოპერაციის სიმბოლო და მეორე რიცხვი მივაბი ერთმანეთს.
    ?>

    <!-- ფორმა -->
    <form action="" method="post">
    <div class="div1">
        <input type="text" class="num1" name="num1" value="<?=$lastresult?>" readonly>
    </div>
    <input type="hidden" name="numbers" value= "<?=$result?>">
    <br>
    <input type="submit" class="reset" value="Reset">
    <br><br>
    <input type="number" name="num" value="Please insert a number...">
    <br><br>
    <input type="submit" name="check" class="check" value="Check">
    <br><br>
    </form>

    <!-- აქ დავწერე ღილაკზე დაჭერის შემდეგ გამოსატანი შედეგი. -->
    <?php
    if(isset($_POST['check'])){
        if($_POST['num'] == $_POST['numbers']){ // 'num' სადაც უნდა ჩავწეროთ პასუხი, გავუტოლე ზემოთ დაწერილი ცვლადის 'input'-ს, რომელიც ასრულებს მიმატებას ან გამოკლებას.
            echo '<font color="green"><b>სწორია!</b></font>';
        }else{
            echo '<font color="red"><b>არასწორია!</b></font>';
        }
    }
    ?>
</body>
</html>