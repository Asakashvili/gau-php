<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task_1</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Task_1</h1>
    <p>10x10 table with random numbers in the range of [10; 99]</p>
    <?php
    function tbl(){ // დავწერე ფუნქცია და დავარქვი სახელი.
        echo "<table>"; // ფუნქციაში გავხსენი 'table' თეგი ცხრილისთვის.
        for($col=0; $col<10; $col++){ // 'table' თეგში შემოვიტანე '10' სვეტი.
            echo "<tr>"; // გავხსენი 'tr' თეგი.
            for($row=0; $row<10; $row++){ // შემოვიტანე '10' რიგი.
                echo "<td>"; // გავხსენი 'td' თეგი.
                echo rand(10, 99); // 'td' თეგებში დავბეჭდე შემთხვევითი რიცხვი '10'-დან '99'-ის ჩათვლით.
                echo "</td>"; // დავხურე 'td' თეგი.
            }
            echo "</tr>"; // დავხურე 'tr' თეგი.
        }
        echo "</table>"; // დავხურე 'table' თეგი.
    }

    tbl(); // გამოვიძახე ფუნქცია რომ საბოლოოდ ჩაიტვირთოს და იმუშაოს.
    ?>
</body>
</html>