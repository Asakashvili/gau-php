<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task_9</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <!-- სათაური -->
    <h1>Task_9</h1>
    <p>password strength identifier</p>

    <!-- ფუნქცია -->
    <?php
    function sidentifier($password){ // დავწერე ფუნქცია და დავარქვი სახელი.
        // აქ განვსაზღვრე სხვადასხვა დონის პაროლის სიმძლავრე;
        $count = 0;
        if(strlen($password) > 8);
        if(preg_match("~(?=\S*[A-Z])~", $password)) $count++;
        if(preg_match("~(?=\S*[\d])~", $password)) $count++;
        if(preg_match("~(?=\S*[\W])~", $password)) $count++;
        
        // აქ განვსაზღვრე სხვადასხვა ქეისში ინდივიდუალური ტექსტის გამოტანა.
        switch($count){
            case 0: 
                echo "<font color='red'><b>Very Weak!</b></font>"; 
                break;
            case 1:
                echo "<font color='red'><b>Weak!</b></font>"; 
                break;
            case 2:
                echo "<font color='yellow'><b>Normal!</b></font>"; ; 
                break;
            case 3:
                echo "<font color='green'><b>Strong!</b></font>"; 
                break;
        }
    }
    ?>

    <!-- ღილაკის დაჭერის შემდეგ -->
    <?php
        $text = "";
        if (isset($_POST['check'])) {
            echo sidentifier($_POST['pw']); // აქ მივუთითე ზემოთ დაწერილი ფუნქციის სახელი.
            $text = $_POST['pw']; // გადავეცი პაროლის 'field'-ის სახელი.
        }
    ?>

    <!-- ფორმა -->
    <form action="" method="post">
        <input type="text" name="pw" placeholder="Enter a password..." value="<?=$text?>">
        <br><br>
        <input type="submit" class="check" name="check" value="Check">
    </form>
</body>
</html>