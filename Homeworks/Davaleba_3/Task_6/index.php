<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task_6</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

    <!-- სათაური -->
    <h1>Task_6</h1>
    <p>table generator</p>

    <!-- ფორმა -->
    <form action="" method="post">
        <label for="M">Enter the amount of Rows(M):</label>
        <br>
        <input type="number" name="M">
        <br><br>
        <label for="N">Enter the amount of Columns(N):</label>
        <br>
        <input type="number" name="N">
        <br><br>
        <label for="a">Enter minimum number(a):</label>
        <br>
        <input type="number" name="a">
        <br><br>
        <label for="b">Enter maximum number(b):</label>
        <br>
        <input type="number" name="b">
        <br><br>
        <input type="submit" name="submit" class="submit" value="Submit">
    </form>
    <br>
    
    <!-- ღიალკზე დაჭერის შემდეგ -->
    <?php
        if (isset($_POST['submit'])) {
            echo "<table>";
            for ($row = 0; $row < $_POST['M']; $row++) {
                $row_count = 0;
                $col_count = 0;
                echo "<tr>";
                for ($col = 0; $col < $_POST['N']; $col++) {
                    $num = rand($_POST['a'], $_POST['b']);
                    echo "<td>$num</td>";
                    $row_count += $num;
                }
                echo "<th>Sum of this Row is: $row_count</th>"; // საბოლოოდ გამოიტანს თითოეულ რიგზე შეტანილი რიცხვების ჯამს ამავე რიცხვების გასწვრივ.
                echo "</tr>";
            }
            echo "</table>";
        }
    ?>
</body>
</html>