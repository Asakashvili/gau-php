<?php
if (isset($_POST['submit'])){
    // აქ განვსაზღვრე ფონტის ფერები.
    $err_color = "red";
    $message_color = "green";

    // აქ ერორის და მესიჯის ცვალდები გავუტოლე სიცარიელეს.
    $extension_error = "";
    $size_error = "";
    $message = "";

    // ფაილის ასატვირთი სკრიპტი.
    $target_dir = "uploads/"; // აქ განვსაზღვრე კონკრეტული საქაღალდე სადაც ატვირთვის შემდეგ კონკრეტული ფაილი განთავსდება.
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]); // განვსაზღვრე ფაილის ასატვირთი ლოკაცია.
    $uploadOk = 1; // ამას შემდეგში ვიყენებთ ფაილის წარმატებით/წარუმატებლად ატვირთვის დასატესტად.
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION)); // პატარა სიმბოლოებით ინახავს ფაილის გაფართოებას.

    // ფაილის გარკვეული გაფართოებების დაშვება.
    if ($imageFileType != "png" && $imageFileType != "jpg" && $imageFileType != "gif"){
        $extension_error = "* გთხოვთ აირჩიოთ მხოლოდ .png, .jpg ან .gif გაფართოების ფაილები!";
        $uploadOk = 0;
    }else{
        $message = "გილოცავთ, თქვენი ფაილი წარმატებით აიტვირთა!";
        $uploadOk = 1;
    }

    // ფაილისთვის მაქსიმალური ასატვირთი ზომის მინიჭება.
    if ($_FILES["fileToUpload"]["size"] > 100000000){ // 100000000 ბაიტი  = 100 მეგაბაიტს.
        $size_error = "* თქვენს მიერ არჩეული ფაილის ზომა აღემატება მაქსიმალურ ფაილის ზომას - 100mb-ს!"; // ამ ერორის სწორად გამოსატანად 'php.ini'-ში გავზარდე 'post_max_size' 40M-დან 400M-მდე.
        $uploadOk = 0;
    }else{
        $message = "გილოცავთ, თქვენი ფაილი წარმატებით აიტვირთა!";
        $uploadOk = 1;
    }

    // თუ ყველაფერი კარგადაა.
    if($extension_error == "" && $size_error == ""){ // თუ ორივე ცარიელია, არაფერი არ მოხდა.
        move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], "uploads/".$_FILES["fileToUpload"]["name"]); // აქ მივუთითეთ ლოკაცია ფაილის ასატვირთად.
        // თუ ყველაფერი ნორმაშია მაგ შემთხვევაში წარმატებით აიტვირთება.
        $message = "გილოცავთ, თქვენი ფაილი წარმატებით აიტვირთა!";
        }

    // ბოლოს გამოვიტანე შესაბამისი ტექსტები ფაილის წარმატებლად/წარუმატებლად ატვირთვის შემთხვევაში.
    echo "<br>";
    echo '<div style="font-weight: bold; color:'.$message_color.'">'.$message.'</div>';
    echo '<div style="font-weight: bold; color:'.$err_color.'">'.$extension_error.'</div>';
    echo '<div style="font-weight: bold; color:'.$err_color.'">'.$size_error.'</div>';
}
?>