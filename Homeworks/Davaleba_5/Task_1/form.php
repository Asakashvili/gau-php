<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <!-- სათაური -->
    <h1>Task_1</h1>
    <p>upload certain file formats, limit the upload size</p>

    <!-- ფორმა -->
    <form method="post" enctype="multipart/form-data">
        <label for="fileToUpload">Upload File <i>(.png, .jpg, or .gif files only)</i>:</label>
        <br><br>
        <!-- 'accept'-ის მეშვეობით მომხარებერლს ასატვირთად მხოლოდ ქვემოთ მოცემული ფაილები გამოუჩნდება. -->
        <input type="file" name="fileToUpload" accept=".png, .jpg, .gif">
        <br>
        <input type="submit" name="submit" class="submit" value="Upload">
    </form>

    <!-- მივაბი კონკრეტული ფაილი რომელშიც სხვადასხვა ოპერაციები მოვახდინე -->
    <?php
    include "upload.php";
    ?>
</body>
</html>