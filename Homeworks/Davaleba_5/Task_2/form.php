<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <!-- სათაური -->
    <h1>Task_2</h1>
    <p>limit file size, download and delete uploaded files</p>

    <!-- ფორმა -->
    <form method="post" enctype="multipart/form-data">
        <label for="fileToUpload">Upload File:</label>
        <br><br>
        <input type="file" name="fileToUpload">
        <br><br>
        <label for="delFile">Delete File <i>(type in file name with its extension)</i>:</label>
        <br><br>
        <input type="text" name="filename">
        <br><br>
        <input type="submit" name="submit" class="submit" value="Upload">
        <br>
        <input type="submit" name="delete" class="delete" value="Delete">
    </form>
    <br>

    <!-- დავწერე 'div'-ი და მივაბი ფაილი რომელშიც შევასრულე სხვადასხვა ოპერაციები ფაილის გადმოსაწერად. -->
    <p>downloadables down below if file exists in certain directory</p>
    <div class="div1">
        <?php
        include "download.php";
        ?>
    </div>

    <!-- მივაბი კონკრეტული ფაილი რომელშიც სხვადასხვა ოპერაციები მოვახდინე -->
    <?php
    include "upload.php";
    ?>
</body>
</html>