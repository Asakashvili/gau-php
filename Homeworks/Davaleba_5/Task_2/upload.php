<?php
// აქ გლობალურად განვსაზღვრე ფონტის ფერები.
$err_color = "red";
$message_color = "green";

// აქ გლობალურად ერორის და მესიჯის ცვალდები გავუტოლე სიცარიელეს.
$size_error = "";
$del_error = "";
$message1 = ""; // <== ფაილის ზომისთვის.
$message2 = ""; // <== ფაილის წაშლისთვის.

if (isset($_POST['submit'])){
    // ფაილის ასატვირთი სკრიპტი.
    $target_dir = "uploads/"; // აქ განვსაზღვრე კონკრეტული საქაღალდე სადაც ატვირთვის შემდეგ კონკრეტული ფაილი განთავსდება.
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]); // განვსაზღვრე ფაილის ასატვირთი ლოკაცია.
    $uploadOk = 1; // ამას შემდეგში ვიყენებთ ფაილის წარმატებით/წარუმატებლად ატვირთვის დასატესტად.
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION)); // პატარა სიმბოლოებით ინახავს ფაილის გაფართოებას.

    // ფაილისთვის მაქსიმალური ასატვირთი ზომის მინიჭება.
    if ($_FILES["fileToUpload"]["size"] > 50000000){ // 50000000 ბაიტი  = 50 მეგაბაიტს.
        $size_error = "* თქვენს მიერ არჩეული ფაილის ზომა აღემატება მაქსიმალურ ფაილის ზომას - 50mb-ს!"; // ამ ერორის სწორად გამოსატანად 'php.ini'-ში გავზარდე 'post_max_size' 40M-დან 400M-მდე.
        $uploadOk = 0;
    }else{
        $message = "გილოცავთ, თქვენი ფაილი წარმატებით აიტვირთა!";
        $uploadOk = 1;
    }

    // თუ ყველაფერი სწორადაა.
    if ($size_error == ""){ // თუ ცარიელია, არაფერი არ მოხდა.
        move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], "uploads/".$_FILES["fileToUpload"]["name"]); // აქ მივუთითეთ ლოკაცია ფაილის ასატვირთად.
        // თუ ყველაფერი ნორმაშია მაგ შემთხვევაში წარმატებით აიტვირთება.
        $message1 = "გილოცავთ, თქვენი ფაილი წარმატებით აიტვირთა!";
        }

    // ბოლოს გამოვიტანე შესაბამისი ტექსტები ფაილის წარმატებლად/წარუმატებლად ატვირთვის შემთხვევაში.
    echo "<br>";
    echo '<div style="font-weight: bold; color:'.$message_color.'">'.$message1.'</div>';
    echo '<div style="font-weight: bold; color:'.$err_color.'">'.$size_error.'</div>';
}

// აქ დავწერე ფაილის წასაშლელი კოდი.
if (isset($_POST['delete'])){
    $fileName = $_POST['filename']; // ახლიდან განვსაზღვრე ცვლადი.

    // აქ შევამოწმე არსებობს თუ არა კონკრეტული ფაილი (გაფართოებით).
    if (file_exists("uploads/".$fileName))
    {
        unlink("uploads/".$fileName); // 'unlink'-ის მეშვეობით წავშალე კონკრეტული ფაილი.
        $message2 = "გილოცავთ, თქვენს მიერ მითითებული ფაილი წარმატებით წაიშალა!";
        }else{
            $del_error = "ფაილი ვერ მოიძებნა!";
            }
        }

        // ბოლოს გამოვიტანე შესაბამისი ტექსტები ფაილის წარმატებლად/წარუმატებლად ატვირთვის შემთხვევაში.
        echo "<br>";
        echo '<div style="font-weight: bold; color:'.$message_color.'">'.$message2.'</div>';
        echo '<div style="font-weight: bold; color:'.$err_color.'">'.$del_error.'</div>';
?>