<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registration Form</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Task_1</h1>
    <p>registration form</p>
    <?php
        // თავდაპირველად განვსაზღვრე ცვლადები ინდივიდუალურად ერორ ტექსტებისთვის.
        $email_error = "";
        $password_error = "";
        $rpassword_error = "";
        $info_error = "";
        $success = "";

        // აქ ცალკე გლობალურად განვსაზღვრე იგივე ცვლადები და გავუტოლე სიცარიელეს, ანუ დასაბმითების შემდეგ მონაცემები ველში დარჩება და არ წაიშლება.
        $email = "";

        if(isset($_POST['submit'])){
            // განვსაზღვრე ცალკეული ცვლადები რომლებსაც დავუკავშირე/გადავეცი ინფუთის სახელები.
            $email = $_POST['email'];
            $password = $_POST['password'];
            $rpassword = $_POST['rpassword'];
            $info = $_POST['info'];
            $word = $_POST['word'];

            // აქ იმეილის ველი გავხადე სავალდებულო.
            if (empty($_POST["email"])){
                $email_error = "* E-Mail is required"; // აქ დავწერე თუ რა გამოიტანოს ერორად 'span'-ში მითითებულმა ცვლადმა.
                }else{
                    $email = ($_POST["email"]); // ამ შემთხვევაში მონაცემებს ჩათვლის სწორად შეყვანილად.
                    
                    // აქ კი შევამოწმე შეიცავს თუ არა საჭირო სიმბოლოებს.
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
                        $email_error = "* E-Mail is NOT valid!";
                        }
                    }

                    // აქ პაროლის ველში სავალდებულო გავხადე რიცხვებისა და სიმბოლოების შეტანა.
                    if (empty($_POST["password"])){
                        $password_error = "* Password is required";
                        }else{
                            $password = ($_POST["password"]); // ამ შემთხვევაში მონაცემებს ჩათვლის სწორად შეყვანილად.

                            // აქ 'preg_match'-ის მეშვეობით მივუთითე რომ პაროლი აუცილებლად უნდა შეიცავდეს რიცხვებსა და სიმბოლოებს.
                            if (!preg_match('@[A-z][0-9]@', $password)){ // დიდი და პატარა სიმბოლოების მითითების შემთხვევაში ორივეს ჩათვლის.
                                $password_error = "* Password must contain both NUMBERS and SYMBOLS!";
                                }
                            }

                            // აქ შევამოწმე პაროლები ემთხვევიან თუ არა ერთმანეთს.
                            if (empty($_POST["rpassword"])){
                                $rpassword_error =  "* Confirm your password!";
                            }else{
                                $rpassword = ($_POST["rpassword"]); // ამ შემთხვევაში მონაცემებს ჩათვლის სწორად შეყვანილად.

                                // აქ დავწერე თუ პირველი პაროლის ცვლადი არ უდრის მეორესას ესეიგი ისინი არ ემთხვევიან ერთმანეთს, შესაბამისად გამოიტანს ერორს.
                                if ($password != $rpassword){
                                    $rpassword_error = "* Passwords does NOT MATCH!";
                                }
                            }

                            // აქ შევამოწმე უსაფრთხოების კოდი ემთხვევა თუ არა მომხმარებლის მიერ შეყვანილ კოდს.
                            if (empty($_POST["info"])){
                                $info_error = "* CAPTCHA is required";
                            }else{
                                $info = ($_POST["info"]); // ამ შემთხვევაში მონაცემებს ჩათვლის სწორად შეყვანილად.
                                
                                // აქ შევამოწმე სწორია თუ არა მომხმარებლის მიერ შეყვანილი უსაფრთხოების კოდი.
                                if ($info != $word){
                                    $info_error = "* CAPTCHA that you inserted is incorrect!";
                                }
                            }
                        }
                        ?>

    <?php
    $randSymbols = rand(10000, 99999); // აქ განვსაზღვრე 5 შემთხვევით რიცხვიანი ცვლადი უსაფრთხოების კოდისთვის.
    ?>

    <form method="post">
        <label for="email">E-Mail:</label>
        <br>
        <input type="text" name="email" placeholder="Insert an e-mail..." value="<?php echo $email;?>">
        <span class="error"><?php echo $email_error;?></span> 
        <br><br>
        <label for="password">Password:</label>
        <br>
        <input type="password" name="password" placeholder="Insert your password...">
        <span class="error"><?php echo $password_error;?></span> 
        <br><br>
        <label for="rpassword">Repeat Password:</label>
        <br>
        <input type="password" name="rpassword" placeholder="Repeat your password...">
        <span class="error"><?php echo $rpassword_error;?></span> 
        <br><br>
        <label for="word">CAPTCHA:</label>
        <div class="div1">
            <!-- 'input'-ში 'value'-დ შევიტანე შემთხვევითი რიცხვები და ეს 'input' გავხადე 'readonly'. -->
            <input name="word" class="word" value="<?=$randSymbols?>" readonly>
        </div>
        <br><br>
        <input type="text" name="info" placeholder="Insert the captcha...">
        <span class="error"><?php echo $info_error;?></span> 
        <br><br>
        <input type="submit" name="submit" class="submit">
    </form>
</body>
</html>