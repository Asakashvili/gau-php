<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Task_2</h1>
    <p>files - [create directory, type in the file, name the files both automatically & mechanically]</p>
    <?php
    // შევამოწმე ფოლდერის სახელი, წარმატებით დასრულების შემთხვევაში კი 'mkdir'-ით შევქმენი ფოლდერი და მივანიჭე/ჩავუწერე სახელი.
    if (!is_dir("files")){
        mkdir("files");
    }

    if (isset($_POST['submit'])){
        $fileName = $_POST['filename']; // განვსაზღვრე ცვლადი და გადავეცი მისივე სახელი ქვემოთ დაწერილი ფორმიდან.
        $content = $_POST['content']; // განვსაზღვრე ცვლადი და გადავეცი მისივე სახელი ქვემოთ დაწერილი ფორმიდან.
       
        // აქ დავწერე რა მოხდება იმ შემთხვევაში თუ კი ფაილის სახელის ველს ცარიელს დავტოვებ.
        if(empty($fileName)){
            $fileName = date("Y-m-d-H-i-s").".txt"; // ავტომატურად დაერქმევა მოცემული სახელი.
        }else{
            // აქ მივაბი 'date' რაც ჩვენს მიერ მითითებულ სახელს ავტომატურად წინ დაუწერს ფაილის შექმნის დროს.
            // წელი - თვე - დღე - საათი - წუთი - წამი.
            $fileName = date("Y-m-d-H-i-s")."---".$fileName.".txt"; // მითითების შემთხვევაში კი მიენიჭება ჩვენს მიერ შეყვანილი სახელი, ასევე მივაბი '.txt' რომ ფაილი აუცილებლად იყოს '.txt' გაფართოების.
        }

        $files = scandir("files"); // ახდენს სკანირებას / განლაგებას.

        $f = fopen("files/".$fileName, 'w'); // აქ მივუთითე ფაილის შექმნის ლოკაცია და მივაბი ფაილის სახელის ცვლადი. ასევე დავუწერე 'w' რაც ნიშნავს დაწერას.
        fwrite($f, $content); // აქ მივუთითე რომელი ველიდან შეიძლება ფაილში ტექსტის შეტანა.
    }
    ?>
    
    <form method="post">
        <label for="filename">File Name:</label>
        <input type="text" name="filename" placeholder="Insert file name...">
        <br><br>
        <label for="content">File Content:</label>
        <br>
        <textarea name="content" cols="50" rows="20" placeholder="Type here..."></textarea>
        <br><br>
        <input type="submit" name="submit" class="submit" value="Create File">
    </form>
</body>
</html>