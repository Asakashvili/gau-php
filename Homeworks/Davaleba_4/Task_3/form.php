<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Task_3</h1>
    <p>files - [create directory, create files in the directory (.txt), type in the files, delete files]</p>
    <?php
    if (isset($_POST['folername']))
    if (!is_dir("directory")){
        mkdir("directory");
    }

    if (isset($_POST['submit'])){
        $fileName = $_POST['filename']; // განვსაზღვრე ცვლადი და გადავეცი მისივე სახელი ქვემოთ დაწერილი ფორმიდან.
        $content = $_POST['content']; // განვსაზღვრე ცვლადი და გადავეცი მისივე სახელი ქვემოთ დაწერილი ფორმიდან.
       
        // აქ დავწერე რა მოხდება იმ შემთხვევაში თუ კი ფაილის სახელის ველს ცარიელს დავტოვებ.
        if(empty($fileName)){
            $fileName = $fileName.".txt"; // ავტომატურად დაერქმევა მოცემული სახელი.
        }else{
            // აქ მივაბი 'date' რაც ჩვენს მიერ მითითებულ სახელს ავტომატურად წინ დაუწერს ფაილის შექმნის დროს.
            // წელი - თვე - დღე - საათი - წუთი - წამი.
            $fileName = $fileName.".txt"; // მითითების შემთხვევაში კი მიენიჭება ჩვენს მიერ შეყვანილი სახელი, ასევე მივაბი '.txt' რომ ფაილი აუცილებლად იყოს '.txt' გაფართოების.
        }

        $files = scandir("directory"); // ახდენს სკანირებას / განლაგებას.

        $f = fopen("directory/".$fileName, 'w'); // აქ მივუთითე ფაილის შექმნის ლოკაცია და მივაბი ფაილის სახელის ცვლადი. ასევე დავუწერე 'w' რაც ნიშნავს დაწერას.
        fwrite($f, $content); // აქ მივუთითე რომელი ველიდან შეიძლება ფაილში ტექსტის შეტანა.
    }

    // აქ 'isset'-ის გარეთ გლობალურად განვსაზღვრე ცვლადები რომ ფორმიდან ხელმისაწვდომი ყოფილიყო.
    $error = "";
    $message = "";

    if(isset($_POST['del']))
    {
        $fileName = $_POST['filename']; // ახლიდან განვსაზღვრე ცვლადი.
        
        // აქ შევამოწმე არსებობს თუ არა კონკრეტული ფაილი (გაფართოებით).
        if(file_exists("directory/".$fileName.".txt"))
        {
            unlink("directory/".$fileName.".txt"); // 'unlink'-ის მეშვეობით წავშალე კონკრეტული ფაილი.
            $message = "ფაილი წარმატებით წაიშალა";
        }else{
            $error = "ფაილი ვერ მოიძებნა!";
        }
    }
    ?>

    <form method = "post">
        <label for="filename">File Name:</label>
        <input type="text" name="filename" placeholder="Insert file name...">
        <span class="error"><?=$error?></span>
        <br><br>
        <label for="content">File Content:</label>
        <br>
        <textarea name="content" cols="50" rows="20" placeholder="Type here..."></textarea>
        <br><br>
        <input type="submit" name="submit" class="submit" value="Create File">
        <br>
        <input type="submit" name="del" class="submit2" value="Delete File">
        <br>
        <span class="message"><?=$message?></span>
    </form>
</body>
</html>