<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quiz Form</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

    <!-- სათაური -->
    <h1>Quiz Form</h1>
    <p class='p1'>class exercise #1</p>

    <!-- შევადგინე ფორმა / 5 კითხვა - 2 სავარაუდო პასუხი -->
    <form action="" method="post">
        <!-- '<ol>''<li>' თეგებით დავლისთე. -->
        <ol>
            <li>
                <!-- კითხვა -->
                <h3>What does PHP stand for?</h3> 
        
                <!-- პირველი სავარაუდო პასუხო თავისი სახელით და მნიშვნელობით -->
                <div>
                    <input type='radio' name='answer1' value='A'/> 
                    <label for='answer1A'>A) Personal Home Page</label>
                </div>
        
                <!-- მეორე სავარაუდო პასუხო თავისი სახელით და მნიშვნელობით -->
                <div>
                    <input type='radio' name='answer1' value='B'/>
                    <label for='answer1B'>B) Hypertext Preprocessor</label>
                 </div>
            </li>


    	    <li>
                <!-- კითხვა -->
                <h3>What does HTML stand for?</h3>

                <!-- პირველი სავარაუდო პასუხო თავისი სახელით და მნიშვნელობით -->
                <div>
                    <input type='radio' name='answer2' value='A'/>
                    <label for='answer2A'>A) HyperText Markup Language</label>
                </div>

                <!-- მეორე სავარაუდო პასუხო თავისი სახელით და მნიშვნელობით -->
                <div>
                    <input type='radio' name='answer2' value='B'/>
                    <label for='answer2B'>B) HyperText Markdown Language</label>
                </div>
            </li>
        
     
            <li>
            <!-- კითხვა -->
            <h3>What does CSS stand for?</h3>

                <!-- პირველი სავარაუდო პასუხო თავისი სახელით და მნიშვნელობით -->
                <div>
                    <input type='radio' name='answer3' value='A'/>
                    <label for='answer3A'>A) Creative Style Sheets</label>
                </div>
        
                <!-- მეორე სავარაუდო პასუხო თავისი სახელით და მნიშვნელობით -->
                <div>
                    <input type='radio' name='answer3' value='B'/>
                    <label for='answer3B'>B) Cascading Style Sheets</label>
                </div>
            </li>

            <li>
            <!-- კითხვა -->
            <h3>When was PHP invented?</h3>
        
                <!-- პირველი სავარაუდო პასუხო თავისი სახელით და მნიშვნელობით -->
                <div>
                    <input type='radio' name='answer4' value='A'/>
                    <label for='answer4A'>A) 1990</label>
                </div>
        
                <!-- მეორე სავარაუდო პასუხო თავისი სახელით და მნიშვნელობით -->
                <div>
                    <input type='radio' name='answer4' value='B'/>
                    <label for='answer4B'>B) 1994</label>
                </div>
            </li>
        
            <li>
            <!-- კითხვა -->
            <h3>Who is the founder of PHP?</h3>
        
                <!-- პირველი სავარაუდო პასუხო თავისი სახელით და მნიშვნელობით -->
                <div>
                    <input type='radio' name='answer5' value='A'/>
                    <label for='answer4A'>B) Rasmus Lerdorf</label>
                </div>
        
                <!-- მეორე სავარაუდო პასუხო თავისი სახელით და მნიშვნელობით -->
                <div>
                    <input type='radio' name='answer5' value='B'/>
                    <label for='answer5B'>A) Brendan Eich</label>
                </div>
            </li>

        </ol>

        <input type='submit' name='submit' class='submit' value='Submit Quiz'/> 
    </form>

    <?php
    if(isset($_POST['submit'])){ // <== ღილაკზე დაჭერის შემდეგ.
        $answer1 = $_POST['answer1']; // <== განვსაზღვრე ცვლადი და დავარქვი 'answer1', მნიშვნელობაში კი გადავეცი შესაბამისი კითხვის პასუხების სახელი.
	    $answer2 = $_POST['answer2']; // <== განვსაზღვრე ცვლადი და დავარქვი 'answer1', მნიშვნელობაში კი გადავეცი შესაბამისი კითხვის პასუხების სახელი.
	    $answer3 = $_POST['answer3']; // <== განვსაზღვრე ცვლადი და დავარქვი 'answer1', მნიშვნელობაში კი გადავეცი შესაბამისი კითხვის პასუხების სახელი.
        $answer4 = $_POST['answer4']; // <== განვსაზღვრე ცვლადი და დავარქვი 'answer1', მნიშვნელობაში კი გადავეცი შესაბამისი კითხვის პასუხების სახელი.
        $answer5 = $_POST['answer5']; // <== განვსაზღვრე ცვლადი და დავარქვი 'answer1', მნიშვნელობაში კი გადავეცი შესაბამისი კითხვის პასუხების სახელი.
	    $score = 0; // განვსაზღვრე ცვლადი ინკრემენტისთვის, რომელიც გავუტოლე 0-ს, მოგვიანებით კი სწორი პასუხის შემთხვევაში ეს ცვლადი დაიწყებს ზრდას +1-ით.
	
	    if ($answer1 == "B"){$score++;} // ზემოთ განსაზღვრული ცვლადი გავუტოლე სწორი კითხვის მნიშვნელობას - 'value'-ს რომლის შემთხვევაშიც ბოლოს გამოიტანს +1 ქულას.
	    if ($answer2 == "A"){$score++;} // ზემოთ განსაზღვრული ცვლადი გავუტოლე სწორი კითხვის მნიშვნელობას - 'value'-ს. რომლის შემთხვევაშიც ბოლოს გამოიტანს +1 ქულას.
	    if ($answer3 == "B"){$score++;} // ზემოთ განსაზღვრული ცვლადი გავუტოლე სწორი კითხვის მნიშვნელობას - 'value'-ს. რომლის შემთხვევაშიც ბოლოს გამოიტანს +1 ქულას.
        if ($answer4 == "B"){$score++;} // ზემოთ განსაზღვრული ცვლადი გავუტოლე სწორი კითხვის მნიშვნელობას - 'value'-ს. რომლის შემთხვევაშიც ბოლოს გამოიტანს +1 ქულას.
        if ($answer5 == "A"){$score++;} // ზემოთ განსაზღვრული ცვლადი გავუტოლე სწორი კითხვის მნიშვნელობას - 'value'-ს. რომლის შემთხვევაშიც ბოლოს გამოიტანს +1 ქულას.
        echo "<br>";
	    echo "<center><font size='6'>Your Score is <br>$score/5!</font></center>"; // დავწერე ბოლოს რა გამოიტანოს, ინკრემენტი/5-დან ანუ 5 კითხვიდან რამდენს ვუპასუხეთ სწორად.
    }
    ?>
</body>
</html>