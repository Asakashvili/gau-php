<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Lecture_1</title>
</head>
<body>
    <h1>PHP Lecture 1</h1>
    <form action="page_action.php">
        <input type="text" name="firstname"> - Name 
        <br><br>
        <input type="number" name="age"> - Age
        <br><br>
        <input type="submit" value="SEND GET">
    </form>
    <br><br>
    <!-- 'post' მეთოდით "URL"-ში მონაცემები არ გამოჩნდება. -->
    <form action="page_action.php" method="post"> 
        <input type="text" name="firstname"> - Name 
        <br><br>
        <input type="number" name="age"> - Age
        <br><br>
        <input type="submit" value="SEND POST">
    </form>
    <br><br>
    <div>
        var x = 23
        console.log(x)
    </div>
    <div style="font-size: 1.5em; margin: auto; width: 50%; padding: 10px; border: solid">
          <?php
          // მნიშვნელოვანია ';' ყველა სტრიქონის ბოლოში.
          $x = 78;  // $ იგივეა რაც 'var'-ით ცვლადის გამოცხადება.
          $y = "PHP";
          $y1 = true;
          $arr1 = [34, 3.9, "MySQL", false, true];
          echo $x; // 'echo' იგივეა რაც 'console.log'.
          echo "<br>";
          echo "<h1 class='style1'>$y</h1>"; // ორმაგი ბრჭყალები თეგების შიგნით არფუნქციონირებს.
          echo $arr1[2];
          echo "<h2 class='style1'>$arr1[2]</h2>"; // მასივიდან ამოვიღეთ მესამე ელემენტი ანუ 'MySQL'.
          print_r($arr1);
    ?>
    </div>
    <script>
        var x = 23
        console.log(x)
    </script>
</body>
</html>