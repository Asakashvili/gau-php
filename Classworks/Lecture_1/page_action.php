<div style="font-size: 1.5em; margin: auto; width: 50%; padding: 10px; border: solid">
<?php
   $x = $_GET['firstname']; // პირველი მეთოდი.
   $age = $_GET['age'];
   echo "<h1>Info</h1>";
   echo "<h2>$x</h2>"; // პირველი მეთოდი.
   echo "<h2>".$_GET['firstname']."</h2>"; // '.' იგივეა რაც '+' JavaScript-ში. / მეორე მეთოდი.
   echo "<h2>$age</h2>";
   echo "<h2>".$_GET['age']."</h2>";
?>
<hr><hr>
<?php
   $x = $_POST['firstname'];
   $age = $_POST['age'];
   echo "<h1>Info</h1>";
   echo "<h2>$x</h2>";
   echo "<h2>".$_POST['firstname']."</h2>";
   echo "<h2>$age</h2>";
   echo "<h2>".$_POST['age']."</h2>";
?>
</div>
