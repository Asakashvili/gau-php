<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lecture_2</title>
</head>
<body>
    <?php
       // ასოციაციური მასივი, შეგვიძლია ჩვენთვითონ მივუთითოთ ინდექსი. / 'key'-ების გამეორება არ შეიძლება. (person-ების).
       $m6 = ["person1"=>"Davit", "person2"=>"Davit", "person3"=>"Ana"];
       // შერეული მასივი.
       $m7 = ["person1"=>"Davit", "ana", "eka", "belka", "person2"=>"bana", "gana"];
       $m8 = ["person1"=>["Ana", "Dvali", 23, "Web Developer"], "person2"=>["Tengo", "Kakulia"]];
    ?>
    <!-- 'pre' = preformatted text (გამოიტანს ზუსტად იმას რასაც თეგებში ჩავწერთ) -->
    <pre>
        <?php
           print_r($m7);
           echo "<br> associaciuri masivi";
           print_r($m8);
        ?>
    </pre>
    <hr><hr>
    <?php
       // ორგანზომილებიანი მასივი ანუ მასივი მასივში. / შეიძლება იყოს სამგანზომილებიანიც.
       $m5 = [ 
           [3, 4, 5, 9],
           [3, "php"],
           [true, 8, 9.7]
       ]
    ?>
     <pre>
        <?php
           print_r($m5); 
        ?>
    </pre>
    <hr><hr>
    <?php
       $m1 = [23, 94, 394, "Gio", false];
       echo rand(10, 100); // შემთხვევითი რიცხვები 10-დან 100-მდე.
       $m2 = array(4, 55, 4, "Hello");
       $m3 = []; // ცარიელი მასივი.
       $m4 = array(); 
    ?>
    <pre>
        <?php
           print_r($m1); // მასივების გამოტანის პირველი ვარიანტი.
           var_dump($m1); // მასივების გამოტანის მეორე ვარიანტი. (მაგ: რა ტიპის მონაცემებია მასივში).
        ?>
    </pre>
    <div>
        <h3>for</h3>
        <?php
           for($i=0; $i<count($m2); $i++){ // 'length'-ის მაგივრად პიეიჩპიში არის 'count'.
              echo $m2[$i]."<br>";
           }
        ?>
    </div>
    <div>
        <h3>for</h3>
        <?php
           foreach($m1 as $k => $el){
              echo $k." - ".$el."<br>"; // '.' არის გაერთიანება.
           }
        ?>
    </div>
</body>
</html>
