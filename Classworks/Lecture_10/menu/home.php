<h1>HOME</h1>
<table class="datatable">
<!-- '<tbody>' ტეგი გამოიყენება 'HTML' ცხრილის ძირითადი შინაარსის დასაჯგუფებლად. -->
<tbody>
    <!-- '<thead>' 'HTML' ელემენტი განსაზღვრავს სტრიქონების ერთობლიობას, რომელიც განსაზღვრავს ცხრილის სვეტების თავს. -->
    <thead>
    <tr>
        <th>Name</th>
        <th>Lastname</th>
        <th>Birthday</th>
        <th>Mobile</th>
        <th>Address</th>
    </tr>
    </thead>
<!-- 'Connection'-ის გამოყენება -->
<?php
// 'ORDER BY id DESC'-ით ჯერ ახალი მონაცემები გამოვა.
$result = mysqli_query($connection, $select); // პირველს ვწერთ თუ რომელ 'connection'-ზე გვინდა და შემდეგ შესაბამისი მოთხოვნა.
// var_dump($result); // გვიბრუნებს რესურს ტიპის მნიშვნელობას, გამოაქვს ძირითადი ინფორმაცია იმ ცხრილის შესახებ რომელზეც 'select' გავაკეთეთ.
// echo mysqli_num_rows($result); // ეს გვიბრუნებს თუ რამდენი ჩანაწერი არის მონიშნული ჩვენი მოთხოვნის შესაბამისად.
if(mysqli_num_rows($result)>0){
    // გამოვიყენეთ 'whil loop' ოპერატორი კონკრეტული ჩანაწერის პირდაპირ გამოსატანად.
    while($row = mysqli_fetch_assoc($result)){ // '_assoc' ანუ გამოვიტანთ ასოციაციური მასივით რომელიც პრაქტიკაში ყველაზე ხშირად გამოიყენება.
        ?>
        <tr>
            <td><?=$row["name"]?></td>
            <td><?=$row["lastname"]?></td>
            <td><?=$row["birthday"]?></td>
            <td><?=$row["mobile"]?></td>
            <td><?=$row["address"]?></td>
            <!-- <td><a href="?change=edit&&id=<?=$row['id']?>">EDIT</a></td>
            <td><a href="?menu=select&&change=delete&&id=<?=$row['id']?>">DELETE</a></td> -->
        </tr>
    <?php
    }}
    ?>
    </tbody>
    </table>