<h1>SIGN UP</h1>
<form action="" method="post">
    <!-- რაც ბაზაში გვიწერია მაგ სახელებს ვწერთ. -->
    <input type="email" name="email" placeholder="email"> 
    <br><br>
    <!-- სასურველია, რომ სახელები ემთხვეოდეს მონაცემთა ბაზაში ჩაწერილ სახელებს. -->
    <input type="password" name="password" placeholder="password">
    <br><br>
    <input type="password" name="re_password" placeholder="repeat password">
    <br><br>
    <input type="submit" value="SIGN UP" name="signup">
</form>

<?php
if(isset($_POST['signup'])){ // თუ არსებობს, თუ დაჭერილია ღილაკზე.
    $email = $_POST["email"];
    $password = $_POST["password"];
    $re_password = $_POST["re_password"];
    // $mobile = $_POST["mobile"];
    // $address = $_POST["address"];
    // // echo $firstname;

    // რეგისტრაციის დროს, სანამ მონაცემებს ჩავწერთ.
    

    if($password == $re_password){ // თუ მომხმარებლის მიერ შეყვანილი პაროლი უდრის განმეორებით პაროლს, მაშინ წარმატებით უნდა დასრულდეს ჩატვირთვა.
        // წინააღმდეგ შემთხვევაში გამოიტანოს "Passwords DOES NOT match!".
        
        // პაროლის დაფარვა მონაცემთა ბაზაში / დაშიფვრა.
        $password = sha1($salt.$password); // ჩვენს მიერ მითითებული პაროლის წინ დაიწერება 'salt' ცვლადში მითითებული ტექსტი. / 'sha1' პაროლს წინ უწერს რაღაც შემთხვევით სიმბოლოებს (ჰეშს).
        // ანუ ზემოთ შეგვიძლია დავწეროთ მხოლოდ '$salt.$password' მაგრამ პაროლის უფრო რთულად წარმოსადგენად ვუწერთ 'sha1'-საც.
        $query = "INSERT INTO admin(email, password) VALUES('$email', '$password')";      
    //  ვამოწმებთ მონაცემები ჩაიწერა თუ არა.
    if(mysqli_query($connection, $query)){
        echo "record added!!!";
        // ლინკისავითაა, ამის მეშვეობით გვერდის დარეფრეშების შემდგომ მონაცემები ავტომატურად აღარ ჩაიწერება.
        header("location: index.php?menu=signin"); // "Sign Up"-ის შემდეგ გადამისამართება მოხდება "Sign In"-ზე.
        echo "Error!!";
    }
  }else{
      echo "Passwords DOES NOT match!";
  }
}
?>