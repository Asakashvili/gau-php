<?php
   $extension_error = "";
   $size_error = "";
   $success_message = "";

   // აქ ვამოწმებთ მომხმარებელმა დააჭირა თუ არა კონკრეტულ ღილაკს რაღაც ფაილის ასატვირთად.
   if(isset($_POST["upload"])) {
       $file_size = $_FILES['fileToUpload']["size"]; // ფაილის ზომა.
       $file_extension = strtolower(pathinfo($_FILES["fileToUpload"]["name"], PATHINFO_EXTENSION)); // ფაილის გაფართოება.
       
       // "!=" - სთან "||"-ს მაგივრად გამოიყენება "&&".
       if($file_extension != "jpg" && $file_extension != "txt"){ // "==" - "არის" / "||" - "ან" // "&&" - "და" / "!=" - "ამისგან განსხვავებულია".
          $extension_error = "Wrong Extension!!!";
       }

       // "||" - to combine two expressions and returns true if either expression is true - otherwise, it returns false.
       // "&&" - the logical operator && returns: TRUE only if both of its operands evaluate to true. FALSE if either or both of its operands evaluate to false.

       if($file_size > 5000){ // ბაიტების შეზღუდვა, 5000 ბაიტზე მეტი თუა.
          $size_error = "Wrong Size!!!";
       }

       // თუ ყველაფერი კარგადაა.
       if($extension_error == "" && $size_error == ""){ // თუ ორივე ცარიელია, არაფერი არ მოხდა.
          move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], "uploads/".time().$_FILES["fileToUpload"]["name"]); // აქ მივუთითეთ ლოკაცია ფაილის ასატვირთად.
          // ".time()" რომ ჩავამატეთ ამის მეშვეობით ერთიდაიმავე ფაილები ატვირთვისას ერთმანეთს არ გადაეწერება და ინდივიდუალურად დაემატება საქაღალდეში.
          // თუ ყველაფერი ნორმაშია მაგ შემთხვევაში წარმატებით აიტვირთება.
          $success_message = "File Uploaded!";
    }

    // echo "<pre>";
    //    print_r($_FILES);
    // echo "</pre>";
    // $fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // 'strtolower'-ს სტრინგი გადაყავს დაბალ რეგისტრში.
    // 'pathinfo' არის მისამართის შესახებ ინფორმაცია.

    // echo "<hr>";
    // print_r(strtolower(pathinfo($_FILES["fileToUpload"]["name"], PATHINFO_EXTENSION))); // 'pathinfo' გვიბრუნებს მასივს, (დირექტორიას, ფაილის სახელს და გაფართოებას).
    // 'strtolower' გამოიყენება იმ შემთხვევისთვის თავიდან ასარიდებლად თუ მომხმარებელმა შემთხვევით ატვირთა არარსებული გაფართოების ფაილი, მაგალითად '.txT'.
    // 'PATHINFO_EXTENSION'-ის ჩამატების შემდეგ დაგვიბრუნებს მხოლოდ ფაილის გაფართოებას.
    // echo "<hr>";
    // ესენი დავწერეთ ფაილის ატვირთვისას შესაბამისი დეტალები / ფაილის ატვირთვისას ეს ორი ფაქტორი უნდა იყოს გათვალისწინებული.
    // echo $_FILES["fileToUpload"]["name"]; // რა ქვია ფაილს და რა გაფართოებისაა (მაგ: .jpg, .png)...
    // echo "<hr>";
    // echo $_FILES["fileToUpload"]["size"]; // გამოიტანს ზომას / შევამოწმოთ თუ რა ზომის ფაილს ვტვირთავთ.
    // echo "<hr>";
  }

  echo $extension_error;
  echo "<br>";
  echo $size_error;
  echo "<br>";
  echo $success_message;
?>
