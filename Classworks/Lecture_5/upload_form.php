<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Upload Form</title>
    <link rel="stylesheet" href="style.css">
</head>
<!-- ფაილების ატვირთვა -->
<body>
    <div class="container">
    <!-- მეთოდად აუცილებლად უნდა ქონდეს 'post', 'enctype' გვეხმარება ფაილის ატვირთვაში, მის გარეშე ეს ფორმა იქნებოდა სრულიად ჩვეულებრივი. -->
    <form method="post" enctype="multipart/form-data">
        Select image to upload:
        <!-- 'type'-ში 'file'-ს მითითებით ჩნდება ფუნქციონალური 'Choose File' ღილაკი. -->
        <!-- 'accept'-ში მივუთითებთ კონკრეტულ ფაილებს რომლებსაც ატვირთვისას დაინახავს. -->
        <!-- ფაილები უნდა აიტვირთოს კონკრეტულ საქაღალდეში. -->
        <input type="file" name="fileToUpload" accept=".jpg"> 
        <input type="submit" value="Upload File" name="upload">
        </form>
    </div>
    <div class="container">
        <?php
           include "upload.php";
        ?>
    </div>
</body>
</html>