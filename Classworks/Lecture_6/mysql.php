<?php
include "connection.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Php MySql</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>PHP - MySQL</h1>

    <!-- 'Connection'-ის გამოყენება -->
    <?php
    $select = "SELECT firstname, lastname FROM users WHERE id > 2"; // გამოიტანს მხოლოდ ორ სახელს და გვარს.
    // ასევე "SELECT firstname, lastname FROM users" და ქვემოთ გადაცემულებს დავაკომენტარებთ, ამით 'users' ცხრილიდან ამოვიღებთ მხოლოდ და მხოლოდ სახელს და გვარს.
    // ასევე "SELECT * FROM users";  // რაც კი არის 'User' ცხრილში ჩაწერილი ყველაფერი გამოვიტანოთ. (სამივე ჩანაწერი).
    // '$select' მოთხოვნის დასამუშავებლად გამოიყენება 'mysqli_query()' მეთოდი:
    $result = mysqli_query($connection, $select); // პირველს ვწერთ თუ რომელ 'connection'-ზე გვინდა და შემდეგ შესაბამისი მოთხოვნა.
    // var_dump($result); // გვიბრუნებს რესურს ტიპის მნიშვნელობას, გამოაქვს ძირითადი ინფორმაცია იმ ცხრილის შესახებ რომელზეც 'select' გავაკეთეთ.
    // echo mysqli_num_rows($result); // ეს გვიბრუნებს თუ რამდენი ჩანაწერი არის მონიშნული ჩვენი მოთხოვნის შესაბამისად.
    if(mysqli_num_rows($result)>0){
        // გამოვიყენეთ 'whil loop' ოპერატორი კონკრეტული ჩანაწერის პირდაპირ გამოსატანად.
        while($row = mysqli_fetch_assoc($result)){ // '_assoc' ანუ გამოვიტანთ ასოციაციური მასივით რომელიც პრაქტიკაში ყველაზე ხშირად გამოიყენება.
            ?>

            <!-- აქ ეს მნიშვნელობები ჩავსვით '<div>' თეგში -->
            <div class="container">
                <div><?=$row['firstname']?></div>
                <div><?=$row['lastname']?></div>

                <!-- ესენი დავაკომენტარე რადგან ზემოთ განვსაზღვრეთ თუ რა საიდან გამოიტანოს -->
                <!-- <div><?=$row['mobile']?></div> -->
                <!-- <div><?=$row['birthday']?></div> -->
                <!-- <div><?=$row['address']?></div> -->
            </div>

            <?php
            // echo "<pre>";
            // print_r($row); // ამოვიღეთ პირველი ჩანაწერი.
            // echo "</pre>";
        }
    }
    ?>
</body>
</html>