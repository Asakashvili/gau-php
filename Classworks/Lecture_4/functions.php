<?php
// ვამოწმებთ '0' არის თუ არა.
   function check_digit($txt){
       $dig = "0123456789";
       $txt = "~".$txt;
       for($i=0; $i<strlen($dig); $i++){ // 'strlen' არის სტრიქონის სიგრძე.
        echo $dig[$i]."<br>"; // // სათითაოდ გამოიტანს ყველა სიმბოლოს.
        if(strpos($txt, $dig[$i])==true){
            echo "<h1>True</h1>";
            return 1; // სადმე 'True' თუ დაფიქსირდება პირდაპირ გამოიტანს.
        }else{
            echo "<h1>False</h1>";
        }
    }
    return 0; // ციკლის ბოლოს.
    // echo $dig[0]; // ამოვიღეთ '0' '$dig' ცვლადიდან.
    // $sym = "d";
    // echo strpos($txt, $sym); // პირველი პარამეტრია სადაც ვეძებთ, მეორე კი რასაც ვეძებთ - არაფერს დააბრუნებს იმიტომ რომ 'false'-ა.
    // if(0){
    //     echo "<h1>True</h1>";
    // }else{
    //     echo "<h1>False</h1>";
    // }
    // აბრუნებს 'check_digit'-დან, თუ სტრიქონში შედის კონკრეტული სიმბოლო დააბრუნებს მის პოზიციას, 'p' შემთხვევაში '0'-ს. 
    
    // if(strpos($txt, $sym)===true){
    //     echo "<h1>True</h1>";
    // }else{
    //     echo "<h1>False</h1>";
    // }
}
$pass = "dp2hp123";
echo check_digit($pass);
echo "<br>";
echo $pass;
?>