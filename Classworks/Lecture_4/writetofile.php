<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Write</title>
</head>
<!-- ფაილებთან მუშაობა -->
<body>
    <?php
    //    die("Unable to open file!"); // <== 'die' გამოიყენება შეტყობინების დასაბეჭდად და მიმდინარე 'PHP' სკრიპტიდან გასასვლელად. 
    ?>
    <h1>Write to file</h1>
    <?php
       // გაშვების შემდეგ თვითონ იქმნება 'txt' ფაილი.
       // 'die' შემთხვევაში თუ რაიმე ფაილი არ იქმნება შეზღუდვების გამო გამოიტანს შესაბამის შეტყობინებას. /ან/.
       // 'fopen' თუ ამუშავდა შეცდომით გაეშვება 'die' და მის შემდეგ აღარაფერი აღარ იტვირთება, ყველა პროცესს აჩერებს.
    //    $myfile = fopen("testfile.txt", "w") or die("Unable to open file!"); // 'myfile' არის იდენტიფიკატორი რომელიც ფაილის მიგნებაში გვეხმარება; რესურსის ტიპის ცვლადი.
    //    echo $myfile;
    //    fwrite($myfile, "Hello World"); // მეორე პარამეტრად ვწერთ რასაც გვინდა და 'Hello WOrld' ჩაიწერება თვითნ მაგ 'txt' ფაილში.
    //    fwrite($myfile, "\n"); // '\n' არის იგივე ენთერი, ახალი ხაზიდან დაწყება.
    //    fwrite($myfile, "Hello PHP");
    //    fwrite($myfile, "Hello Web");
    //    fclose($myfile); // ბოლოს ვხურავთ ფაილს.

    $patch = "files/test.txt";
    $myfile = fopen($patch, "r") or die("Unable to open file");
    echo filesize($patch); // გამოიტანს ფაილში ჩაწერილი სიმბოლოების ბაიტებს, თითო სიმბოლო თითო ბაიტია.
    echo"<hr>";
    // echo fread($myfile, filesize($patch)); // 'fread'-ით მთლიანად ვკითხულობთ ფაილს.
    echo"<hr>";
    // 'fgets' სტრიქონებად წაიკითხავს.
    // echo fgets($myfile); // თუ 'fread' ჩართული დარჩება 'fgets' გამოიტანს არაფერს რადგან ტექსტის შემდეგ წაიკითხავს.
    // echo"<br>";
    // echo fgets($myfile);
    // echo"<br>";
    // echo fgets($myfile);
    // while(!feof($myfile)){ // 'feof' ამოწმებს არის თუ არა მიღწეული ფაილის ბოლო.
    //     echo fgets($myfile) . "<br>";
    // }
    echo "<hr>";
    // echo fgetc($myfile); // 'c' რომ მივუწერთ წაიკითხავს სიმბოლოებად.
    // echo fgetc($myfile);
    while(!feof($myfile)){
        echo fgetc($myfile) . "<br>"; // გამოიტანს ფაილში ჩაწერილი სიტყვების თითოეულ სიმბოლოს ახალ-ახალ ხაზზე.
    }
    fclose($myfile);
    ?>
</body>
</html>