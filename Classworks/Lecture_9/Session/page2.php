<!-- 'SESSION' ყოველთვის უნდა ჩაირთოს ფაილის დასაწყისში. -->
<?php
// ამ ფუნქციის დაწერის გარეშე სესიას ვერ გამოვიყენებთ.
// სესიის გაშვება.
session_start();
?>
<h1>Page 2</h1>
<br><br>
<a href="page1.php">Page1</a>
<br><br>
<a href="page3.php">Page3</a>
<br><br>
<a href="page4.php">Page4</a>
<hr>
<?php
echo "Local -> ".$x1;
echo "<br>";
// აქ სესიის ცვლადი აღარ განვსაზღვრეთ და პირდაპირ გადმოვიტანეთ.
echo "Session -> ".$_SESSION['x2']; // გლობალური ასოციაციური მასივი.
echo "<br>";
echo "Session -> ".$_SESSION['x3'];
echo "<br>";
echo "Session -> ".$_SESSION['x4'];
echo "<br>";

// 'unset' ფუნქციით სესიიდან ამოვშალე კონკრეტული ცვლადი.
unset($_SESSION['x2']);
?>