<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<!-- ვებგვერდის ლოგიკურ ბლოკებად დაყოფა. -->
<body>
    <?php
    include_once "functions/header_functions.php";
    // f1(); // ფუნქცია გამოვიძახეთ;
    // echo "<hr>";
    // f2("PHP", "Beginner"); // გამოვიძახეთ ფუნქცია და გადავეცით პარამეტრები
    // echo "<hr>";
    // f2("HTML"); // '$level'-ის მნიშვნელობა აიღო.
    // echo "<hr>";
    // echo f3(); // 'echo' დაბეჭვდისთვის.
    // echo "<hr>"
    // echo f4("JavaScript", "starter");

    // $m = 89;
    // $m1["name"] = ""; // ცარიელი. 
    // if(isset($m)){ // <== თუ არსებობს მაშინ დაიბეჭდოს / შემოწმება.
    //     echo $m;
    // }else{
    //     $m1["name"] = "Hello";
    // }
    // echo $m1["name"];

    // 'include' არ გვიზღუდავს ერთი და იგივე ფაილის რამოდენიმეჯერ შემოტანას.
    // 'include' ყველაზე ხშირად გამოყენებადი ფუნქციაა.
    //    include_once "blocks/header.php"; // 'once'-ს შემთხვევაში ერთიდაიგივე ფაილს ვერ შემოვიტანთ.
    //    include "blocks/header.php"; 
       include "blocks/nav.php";
    //    require "blocks/nav.php"; 
    //    require "blocks/menu.php"; // 'require'-ს რაიმე არარსებულის ჩაწერის შემთხვევაში გამოიტანს 'Fatal error'-ს.
    //    include "blocks/menu.php";
    //    include "blocks/header.php"; // <== ჰედერი გამოჩნდება ორად.
    //    include_once "blocks/header.php";
       include "blocks/content.php";
    //    include "blocks/footer.php";
    //    include "blocks/header.php";
    //    include_once
    //    require
    //    require_once
    ?>
</body>
</html>