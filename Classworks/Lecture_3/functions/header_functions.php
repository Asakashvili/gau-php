<!-- გვაქვს ოთხი ტიპის ფუნქცია -->
<?php
function f1(){ // პირველი ტიპის ფუნქცია უბრალოდ რაღაცას ბეჭდავს.
    echo "PHP"; 
}

function f2($lan, $level=""){ // ამ ფუნქციას აქვს პარამეტრები, ასევე შეიძლება რაღაც მნიშვნელობაც ჰქონდეს მინიჭებული.
    echo $lan." - ".$level;
}

function f3(){
    // return; // 'php' ინტეპრეტატორს სადაც შეხვდება 'return' ამის შემდეგ აღარაფერი აღარ აღიქმება.
    $lang1 = "Html";
    // return $lang1; 
    $lang2 = "CSS";
    return $lang1." - ".$lang2; // 'return'-ით რაღაც ფუნქციის მნიშვნელობას ვაბრუნებთ.
}

function f4($lan, $level=""){
    return $lan." - ".$level;
}
?>